##########################
#         Create by       #
#                         #
#          Rouxhero       #
##########################

#      Files Fetcher
import os
import datetime
import threading
from typing import Dict, List, Union

from Floder import Floder
from convert import convert


def toDate(time):
    return datetime.date.fromtimestamp(time)


def createDict(listToChange):
    dit = {}
    for item in listToChange:
        dit[item[0]] = item[1]
    return dit


# datetime.date(2021, 1, 20)


class Fetcher:
    dirOption: Dict[str, Union[List[str], int, str]]

    def __init__(self):
        self.checkDir = []
        self.checkData = []
        self.size = 0
        self.sizeDir = {}
        self.dataResult = {}
        self.allDir = []
        self.sizeTotal = {}
        self.cache = 0
        self.fileOption = {"Extension": ".cfg", "Key": "server"}
        self.dirOption = {
            "Ignore": [".git"],
            "MaxChild": -1,
            "Date": -1,
            "name": "",
            "Keys": "resources",
        }
        self.saveOption = {"Path": "E:/Desktop", "Save": "Result"}
        self.process = []
        self.checkF = None

    def load(self):
        self.allDir = []
        self.sizeDir = {}
        print("Preparing Files\nStarting : ", end=":")
        for path, dirs, files in os.walk(self.saveOption["Path"]):
            listDos = path.split("\\")
            print("Complete !\nLoading : " + path, end=":")
            folder = Floder(
                    path,
                    dirs,
                    files,
                    [
                        toDate(os.stat(path).st_mtime),
                        os.stat(path).st_size,
                        "\\".join(listDos[: len(listDos) - 1]),
                    ],
                )
            self.allDir.append(folder)
            self.sizeDir[path] = [folder, os.stat(path).st_size]

        print("\nLoad completed ! " + str(len(self.allDir)) + "Folder Found", end="")
        open("temp.txt", "w").write(str(self.sizeDir))

    # def checkDouble(self):
    #     self.double = {}
    #     print("Fetching multiple folder")
    #     folder: Floder
    #     for folder in self.allDir:
    #         for folder2 in self.allDir:
    #             # print(str((self.allDir.index(folder)*100)/len(self.allDir))+"-"+str((self.allDir.index(folder2)*100)/len(self.allDir)))
    #             if folder.compare(folder2):
    #                 folder.jumaux.append(folder2)
    #     print("Fetch done, now creating data")
    #     for folder in self.allDir:
    #         if len(folder.jumaux) > 0:
    #             self.double[folder] = folder.jumaux

        # print(len(self.double), "Folder found")

    def getSize(self, path):
        print("Calculating size of " + path, end=" :")
        print(convert(self.size))
        if path in self.sizeDir :
            for file in self.sizeDir[path][0].file:
                try:
                    self.size += os.stat(path + "\\" + file).st_size
                    self.sizeTotal[path + "\\" + file]= os.stat(path + "\\" + file).st_size
                except FileNotFoundError as e:
                    print(file, e)
            for dirs in self.sizeDir[path][0].dirs:
                try:
                    self.getSize(path + "\\" + dirs)
                except TypeError as e:
                    print(path, e)
    def getFileFromSize(self,size):
        selected = {}
        if self.sizeTotal != {}:
            for sizeF in self.sizeTotal.values():
                if sizeF>size :
                    file = list(self.sizeTotal.keys())[list(self.sizeTotal.values()).index(sizeF)]
                    selected[file] = sizeF
        else :
            print('Error , size not init ! , run : size')
        return selected
    def calculateSize(self, path):
        self.size = 0
        try:
            self.size += self.getSize(path)
        except TypeError as e:
            print(e)
        return self.size

    def prepareFile(self):
        self.checkData = []
        self.checkDir = []
        dicLambda = {
            "Extension": lambda file,path: file[-len(self.fileOption["Extension"]) :] == self.fileOption["Extension"],
            "Key": lambda file,path: self.fileOption["Key"] in file,
            "Ignore": lambda file,path: all(not x in self.dirOption["Ignore"] for x in path.path.split("\\")),
            "MaxChild": lambda file,path: len(path.path.split("\\"))<=self.dirOption['MaxChild'],
            "Date": lambda file,path: path.data[0]>self.dirOption['Date'],
            "name": lambda file,path: self.dirOption['name'] in path.path.split("\\")[-1] ,
            "Keys":lambda file,path: self.dirOption['Keys'] in path.dirs,
        }
        for key in self.fileOption:
            if (
                    self.fileOption[key] != ""
                    and self.fileOption[key] != ".*"
                    and self.fileOption[key] != "*"
            ):
                self.checkData.append(dicLambda[key])
        for key in self.dirOption:
            if self.dirOption[key] != "" and self.dirOption[key] != -1:
                self.checkDir.append(dicLambda[key])

        self.checkF = (
            lambda file, path: all(test(file, path) for test in self.checkData) and all(
                test(file, path) for test in self.checkDir))
        print(
            str(len(self.checkData) + len(self.checkDir))
            + " Args found ! Sniffer ready"
        )

    def snifferFile(self, data, ids):
        for folder in data:
            for file in folder.file:
                if len(file)>len(self.fileOption['Extension'] ):
                    if self.checkF(file, folder):
                        if not file in self.dataResult:
                            self.dataResult[file] = [folder]
                        else:
                            if not folder in self.dataResult[file]:
                                self.dataResult[file].append(folder)

        self.process[ids] = True

    def launchSniffer(self):
        if self.checkF is not None:
            self.cleanResult()
            lenTot = len(self.allDir)
            temp = self.allDir.copy()
            nbSniffer = lenTot // 500
            self.process = []
            print(nbSniffer, " Sniffer start in :", lenTot, " Folder")
            i = -1
            for i in range(0, nbSniffer):
                self.process.append(False)
                listTemp = temp[0:500]
                temp[:500] = ""
                threading.Thread(
                    target=self.snifferFile,
                    args=(
                        listTemp,
                        i,
                    ),
                ).start()
            self.process.append(False)
            listTemp = temp[:]
            threading.Thread(
                target=self.snifferFile,
                args=(
                    listTemp,
                    i + 1,
                ),
            ).start()

            while not all(self.process):
                pass
            count = 0
            for key in self.dataResult:
                count += len(self.dataResult[key])
            print(str(count) + "File(s) found")
            self.cache = count
        else:
            print("Error , Sniffer not Ready")

    # Clean
    def cleanResult(self):
        self.dataResult = {}
        self.cache = 0
        print("Bot cache Clean")

    def cleanData(self):
        open("save.txt", "w").write("")
        if os.name == "posix":
            os.system("rm " + self.saveOption["Save"] + "/*.*")
        else:
            # del /p .\Result\*.*
            os.system("del /s /p .\\" + self.saveOption["Save"] + "\\*.*")

    # Setters Save
    def setPathFetch(self, path):
        self.sizeTotal = {}
        self.saveOption["Path"] = path

    def setPathSave(self, path):
        self.saveOption["Save"] = path

    # Setters File
    def setFileExtension(self, extend):
        self.fileOption["Extension"] = extend

    def setKeyWord(self, extend):
        self.fileOption["Key"] = extend

    # Setters Dir  {"Ignore":[".git"],"MaxChild":3,"Date":datetime.date(2021, 1, 20)}
    def setIgnoreDir(self, liste):
        self.dirOption["Ignore"] = liste

    def setMaxChild(self, maxChild):
        self.dirOption["MaxChild"] = maxChild

    def setDate(self, day, month, year):
        if day == month and month == year and day == -1:
            self.dirOption["Date"] = -1
        else:
            self.dirOption["Date"] = datetime.date(year, month, day)

    def setContain(self, name):
        self.dirOption["Keys"] = name

    def setName(self, name):
        self.dirOption["name"] = name

    # save
    # def copy(self):
    #     i = -1
    #     for files in self.dataResult :
    #         i+=1
    #         print(str((i*100)/len(self.dataResult))+"%"+" Done")
    #         with open(self.dataResult[files]+"/"+files, "rb") as saveFile:
    #             open(self.pathSave+"/"+files,"wb").write(saveFile.read())
    #     i+=1
    #     print(str((i*100)/len(self.dataResult))+"%"+" Done")

    def save(self):
        txt = ""
        for files in self.dataResult:
            txt += files + " : " + str(self.dataResult[files]) + "\n"
        with open("save.txt", "w") as saveFile:
            saveFile.write(txt)
