# -*- coding: utf-8 -*-
# Created by Rouxhero
import os
from util.console import Console, clear
from language.lang import language

pythonPath = open("../pythonPath","r").read().split('\n')[0]
data = language["fr"]
screen = Console()
screen.addTitle(data["welcome"])


def sniffer():
    try :
        open("../sniffer/main.py","r")
        opens = True
    except FileNotFoundError:
        screen.addEerror('Sniffer {}'.format(data['ErrorI']))
        choix = input('')
        if choix == 'y' or choix == "Y":
            os.system('cd ..;wget https://gitlab.com/Rouxhero/py-easier/-/raw/master/Sn00.zip')
            opens = True
        else :
            screen.addEerror("Operatoin aborted")
            opens = False
    if opens:
        print(data["Launching"]," Sniffer")
    os.system('{} ../sniffer/main.py')


def play():
    pass


def pygraph():
    pass


def setting():
    pass


def wrong(param):
    screen.addEerror("{} : Wrong Command !".format(param))


command = {"Sniffer": sniffer, "Game": play, "Graph": pygraph, "Setting": setting, "None": wrong}
choix = None
while choix != "quit":
    clear()
    screen.show()
    choix = input("Selection : ")
    allChoice = choix.split(';')
    for userChoice in allChoice:
        userChoice = userChoice.rstrip()
        if choix in command:
            screen.addCommand(choix)
            command[choix]()
        else:
            command["None"](choix)
