# -*- coding: utf-8 -*-
# Created by Rouxhero
import os

systemType = os.name
if systemType == "posix":
    clear = lambda: os.system("clear")
    delete = lambda file : os.system("rm -f {}".format(file))
else :
    clear = lambda: os.system("cls")
    delete = lambda file : os.system("del {}".format(file))

run = """@echo off
title Py-Easier
color A
cd %~dp0src/
{} main.py
""".format(open("pythonPath", "r").read().split("\n")[0])

if __name__ == "__main__":
    clear()
    print("Installing")
    # delete("install.bat")
    open("run.bat","w").write(run)
