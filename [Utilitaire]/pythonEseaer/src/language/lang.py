# -*- coding: utf-8 -*-
# Created by Rouxhero
# Main Language
from language.fr import fr
from language.en import en

language = {"fr": fr, "en": en}