# -*- coding: utf-8 -*-
# Created by Rouxhero

import os
from os import get_terminal_size

size = get_terminal_size().columns
LEFT = "left"
RIGHT = "right"
CENTER = "center"


def clear():
    if os.name != "posix":
        os.system('cls')
    else:
        os.system('clear')


class Console:
    def __init__(self):

        self.charLine = size - (1 + size % 2)
        self.history = []

    def addLine(self, text, position):
        if position == "center":
            coef = (self.charLine - 2 - len(text)) // 2
            if len(text) % 2 != 1:
                self.history.append("#" + " " * coef + text + " " + " " * coef + "#")
            else:
                self.history.append("#" + " " * coef + text + " " * coef + "#")
        elif position == "left":
            coef = self.charLine - 2 - len(text)
            self.history.append("#" + text + " " * coef + "#")
        elif position == "right":
            coef = self.charLine - 2 - len(text)
            self.history.append("#" + " " * coef + text + "#")

    def show(self):
        for line in self.history:
            print(line)

    def FullLine(self):
        self.history.append("#" * self.charLine)

    def EmptyLine(self):
        self.history.append("#" + " " * (self.charLine - 2) + "#")

    def addEerror(self, error):
        self.history.append("Error ! :{}".format(error))

    def addCommand(self, cmd):
        self.history.append("User>>{}".format(cmd))

    def addTitle(self, dic):
        self.FullLine();
        self.EmptyLine()
        for key in dic["head"]:
            self.addLine(dic["head"][key], CENTER);
            self.EmptyLine()
        self.FullLine();
        self.EmptyLine()
        for key in dic["menu"]:
            self.addLine(dic["menu"][key], CENTER);
            self.EmptyLine()
        self.FullLine();
        self.show()
