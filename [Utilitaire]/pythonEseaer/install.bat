title Installation
color A
@echo off
cls
echo ##################################################
echo #                  Instalalation                 #
echo #                     Py-Easier                  #
echo ##################################################
set Pyway=C:\Users\temp\AppData\Local\Programs\Python\Python38\python.exe
If not exist %Pyway% (
echo python  not found
goto :install
) else (
echo python  found !
echo %Pyway% > pythonPath
goto :setUpC
)
:install
set /p a=Avez vous python installé ?[Y/n] :

if %a% equ n ( goto installPython)
set /p b=Entrer le chemin d'acces a python38 :

echo %b% > pythonPath
goto setUpS

:setUps
echo Lancement du setup.py
pause
%b% %~dp0src/setup.py
goto exits

:setUpC
echo Lancement du setup.py
pause
%Pyway% %~dp0src/setup.py
goto exits

:installPython
start lib/Installpython.exe

:exits
echo install finish !
pause
exit