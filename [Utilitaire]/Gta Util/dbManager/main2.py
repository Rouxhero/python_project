# -*- coding: utf-8 -*-

import tkinter as tk
import os


import core


def alert():
    showinfo("Coming soon", "Coming soon !")


class Application(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        # Objet gestion db/save
        self.body = core.Core("config.lg")
        self.body.dbConnect()
        # Liste des Frame
        self.info = tk.LabelFrame(self, text="Info")
        self.connection = tk.Frame(self)
        self.main = tk.Frame(self)
        self.board = tk.LabelFrame(self, text="Resultat")
        self.board.grid()
        self.main.grid()
        self.connection.grid()
        # Definition du menu deroulant
        self.setMenu()
        # Init de l'app
        self.start()

    def start(self):
        # Check de la db connectée
        if not self.body.dbStatu():
            tk.Label(
                self.info,
                text="/!\\ Erreur de la connection a la db /!\\  ",
                width=60,
                fg="red",
            ).pack(side=tk.TOP, padx=5)
            self.connect()
        else:
            self.alreadyConnected()
        self.update()
        # #
        # self.labelVariable = tk.StringVar()
        # self.label = tk.Label(self,textvariable=self.labelVariable)
        # self.label.grid(row=7,column=0)

    def update(self):
        self.info.grid(row=0, columnspan=3, column=0)
        self.connection.grid(row=1, column=0)
        self.main.grid(row=1, column=0)
        self.board.grid(row=2, column=0)

    def alreadyConnected(self):
        # Affiche le statu ok de la db
        tk.Label(
            self.info, text="Connection a la base de donnée ok", width=60, fg="green"
        ).pack(side=tk.TOP, padx=5)
        self.showMain()

    def connect(self):

        # Création des titre de connection
        tk.Label(self.connection, text="host : ").grid(row=1, column=0)
        tk.Label(self.connection, text="user : ").grid(row=2, column=0)
        tk.Label(self.connection, text="password : ").grid(row=3, column=0)
        tk.Label(self.connection, text="dataBase :  ").grid(row=4, column=0)
        # Creation des champs pour la connection
        self.hostVar = tk.StringVar()
        self.hostVar.set("localhost")
        self.userVar = tk.StringVar()
        self.userVar.set("root")
        self.passVar = tk.StringVar()
        self.passVar.set("password")
        self.dBVar = tk.StringVar()
        self.dBVar.set("test")
        self.hostEntry = tk.Entry(
            self.connection, textvariable=self.hostVar, bg="white", width="20"
        )
        self.userEntry = tk.Entry(
            self.connection, textvariable=self.userVar, bg="white", width="20"
        )
        self.passEntry = tk.Entry(
            self.connection, textvariable=self.passVar, show="*", bg="white", width="20"
        )
        self.dBEntry = tk.Entry(
            self.connection, textvariable=self.dBVar, bg="white", width="20"
        )
        self.hostEntry.grid(row=1, column=3)
        self.userEntry.grid(row=2, column=3)
        self.passEntry.grid(row=3, column=3)
        self.dBEntry.grid(row=4, column=3)
        tk.Button(self.connection, text="Connect", command=self.tryConnect).grid(
            row=5, column=2
        )
        self.needSave = tk.IntVar()
        tk.Checkbutton(self.connection, variable=self.needSave, text="Permanent").grid(
            row=5, column=3
        )

    def showMain(self):
        tk.Label(self.main, text="Welcome on db Manager !").grid(
            row=0, column=0, columnspan=4
        )
        tk.Button(self.main, text="Check Bank", command=self.checkBank).grid(
            row=1, column=0
        )
        tk.Button(self.main, text="Check Money", command=self.checkMoney).grid(
            row=1, column=1
        )
        tk.Button(self.main, text="Premium users", command=self.getPremium).grid(
            row=1, column=2
        )

    def setMenu(self):
        menubar = tk.Menu(self)

        menu1 = tk.Menu(menubar, tearoff=0)
        menu1.add_command(label="Save logs", command=self.body.saveLogs)
        menu1.add_separator()
        menu1.add_command(label="Quitter", command=self.leave)
        menubar.add_cascade(label="Fichier", menu=menu1)

        menu2 = tk.Menu(menubar, tearoff=0)
        menu2.add_command(label="Edit connection", command=self.editConnection)
        menu2.add_command(label="Clear board", command=self.clearBoard)
        menu2.add_separator()
        menubar.add_cascade(label="Editer", menu=menu2)

        menu4 = tk.Menu(menubar, tearoff=0)
        menu4.add_command(label="Check bank", command=self.checkBank)
        menu4.add_command(label="White liste", command=alert)
        menu4.add_command(label="Black liste", command=alert)
        menubar.add_cascade(label="Serveur", menu=menu4)

        menu3 = tk.Menu(menubar, tearoff=0)
        menu3.add_command(label="A propos", command=alert)
        menubar.add_cascade(label="Aide", menu=menu3)

        self.config(menu=menubar)

    def editConnection(self):
        self.clearScreen()
        self.connect()
        self.update()

    def tryConnect(self):
        # self.label.configure(text='resultat = '+str(eval(entOperation.get())))
        host = self.hostEntry.get()
        user = self.userEntry.get()
        passw = self.passEntry.get()
        db = self.dBEntry.get()
        saves = [False, True][self.needSave.get()]
        connect = {"host": host, "user": user, "pass": passw, "db": db}
        tk.Label(
            self.connection,
            text="Try to connect to db : " + user + "@" + host + " db : " + db,
        ).grid(row=6, column=4)
        self.body.configDb(saves, **connect)
        self.body.dbConnect()
        print(self.body.dbStatu())
        if self.body.dbStatu():
            self.clearScreen()
            self.alreadyConnected()
        else:
            self.clearScreen()
            tk.Label(
                self.info,
                text="/!\\ Erreur de la connection a la db /!\\  ",
                width=60,
                fg="red",
            ).pack(side=tk.TOP, padx=5)
            self.connect()
        self.update()

    # ListeCommandDuGUI
    def checkBank(self):
        liste = self.body.checkBank()
        self.clearBoard()
        for user in range(len(liste)):
            tk.Label(
                self.board, text=liste[user][0] + " : " + str(liste[user][1])
            ).grid(row=user, column=0, sticky=tk.W)
        self.update()

    def checkMoney(self):
        liste = self.body.checkMoney()
        self.clearBoard()
        for user in range(len(liste)):
            tk.Label(
                self.board, text=liste[user][0] + " : " + str(liste[user][1])
            ).grid(row=user, column=0, sticky=tk.W)
        self.update()

    def getPremium(self):
        liste = self.body.getPremium()
        self.clearBoard()
        for user in range(len(liste)):
            tk.Label(
                self.board, text=liste[user][0] + " : " + str(liste[user][1])
            ).grid(row=user, column=0, sticky=tk.W)
        self.update()

    def clearScreen(self):
        self.info.destroy()
        self.connection.destroy()
        self.info = tk.LabelFrame(self, text="Info")
        self.connection = tk.Frame(self)
        self.connection.grid()
        self.update()

    def clearBoard(self):
        self.board.destroy()
        self.board = tk.LabelFrame(self, text="Resultat")
        self.board.grid()
        self.update()

    def leave(self):
        self.body.saveLogs()
        self.destroy()


if __name__ == "__main__":
    app = Application()
    app.title("DataBase Manager")
    app.geometry("445x560")
    ##----- Programme principal -----##
    app.mainloop()  # Boucle d'attente des événements
    print(app.body.logs.show())
