# -*- coding: utf-8 -*-

# Class core for the main process
import time
import log, db, file


class Core:
    def __init__(self, configPath):
        self.logs = log.Logs()
        self.dbconfigPath = configPath
        config = file.Files("data.lg", True)
        whitelist = file.Files("whitelist.lg", True)
        blacklist = file.Files("blacklist.lg", True)
        self.logs.addLog(config.init())
        res = config.getAllOption()
        self.config = res[1]
        self.logs.addLog(res[0])
        self.db = db.dbManager(self.dbconfigPath)

    def dbStatu(self):
        # Statu of db config
        return self.db.isConnected

    def dbConnect(self):
        # Connect to db
        res = False
        # If config file is allready ok and db allready try
        if self.config["DbConnected"]:
            res = self.connect()
        else:
            res = self.configDb(False)
        if self.dbStatu():
            self.logs.addCmd("Testing db")
            param = {"arg": ["*"], "tab": "users", "where": []}
            self.logs.addLog(self.db.getFromOneTable(**param)[0])
        return res

    def connect(self):
        statu1 = self.db.isConfig()
        self.logs.addLog(statu1[0])  # For log
        if statu1[1]:
            statu2 = self.db.connectDB()
        else:
            statu2 = statu1
        self.logs.addLog(statu2[0])  # For log
        return statu2[1]

    def configDb(self, boolean, **config):
        self.db.putConfig(boolean, **config)
        self.logs.addLog(self.db.isConfig()[0])
        self.connect()

    # Command possible

    def checkBank(self):
        if self.dbStatu():
            self.logs.addCmd("Get all user with bank > 40 000")
            param = {
                "arg": ["name,bank"],
                "tab": "users",
                "where": [["bank", ">", "40000 ORDER BY bank DESC"]],
            }
            res1 = self.db.getFromOneTable(**param)
            self.logs.addLog(res1[0])
            return res1[1]

    def checkMoney(self):
        if self.dbStatu():
            self.logs.addCmd("Get all user with money > 40 000 ")
            param = {
                "arg": ["name,money"],
                "tab": "users",
                "where": [["money", ">", "40000 ORDER BY money DESC"]],
            }
            res1 = self.db.getFromOneTable(**param)
            self.logs.addLog(res1[0])
            return res1[1]

    def getPremium(self):
        if self.dbStatu():
            self.logs.addCmd("Get all Premium user ")
            param = {
                "arg": ["users.name,user_accounts.money"],
                "tab": "user_accounts JOIN users ON users.identifier = user_accounts.identifier",
                "where": [
                    [	"user_accounts.money", 
                    	">",
                    	"0"
                    ],
                    [
                        "user_accounts.name",
                        "=",
                        "'corps' ORDER BY user_accounts.money DESC",
                    ],
                ],
            }
            res1 = self.db.getFromOneTable(**param)
            self.logs.addLog(res1[0])
            return res1[1]

    def saveLogs(self):
        logs = ""
        for line in self.logs.getHistory():
            logs += line + "\n"
        print(logs)
        open("log/" + self.logs.getDate() + ".txt", "w").write(logs)
