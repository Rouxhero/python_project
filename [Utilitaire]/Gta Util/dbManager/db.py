import os
import file, utile

try:
    import mysql.connector
except ModuleNotFoundError:
    os.system("pip install mysql-connector-python")


class dbManager:
    def __init__(self, config):
        self.db = None
        self.Connect = []
        self.isConnected = False
        self.cursor = None
        self.configPath = config
        self.config = file.Files(config, True).getAllOption()[1]

    def isConfig(self):
        if (
            "host" in self.config
            and "user" in self.config
            and "pass" in self.config
            and "db" in self.config
        ):
            return [
                "[DataBase][Succes] config loaded : "
                + self.config["user"]
                + "@"
                + self.config["host"]
                + ", db : "
                + self.config["db"],
                True,
            ]
        else:
            return ["[DataBase][Error] : Config format error !", False]

    def loadConfig(self, configPath):
        if self.isConnected:
            self.quit()
        self.config = file.Files(config, True).getAllOption()[1]
        return isConfig()

    def putConfig(self, boolean, **config):
        if self.isConnected:
            self.quit()
        if (
            "host" in config
            and "user" in config
            and "pass" in config
            and "db" in config
        ):
            self.config = config
            if self.isConfig()[1] and boolean:
                files = file.Files(self.configPath, True)
                for key in config:
                    files.addOption(key, config[key], key)
                files.saveFiles()
            return self.isConfig()
        else:
            return ["[DataBase][Error] Config gived format Error", False]

    def connectDB(self):
        if self.isConfig()[1]:
            try:
                self.conn = mysql.connector.connect(
                    host=self.config["host"],
                    user=self.config["user"],
                    password=self.config["pass"],
                    database=self.config["db"],
                )
            except mysql.connector.errors.ProgrammingError:
                return ["[DataBase][Connection Error] Connection failed :", False]
            self.cursor = self.conn.cursor()
            self.isConnected = True
            return ["[DataBase][Succes] Connection etablished !", True]
        else:
            return ["[DataBase][Error] Config Error, connection failed"]

    def getFromOneTable(self, **param):
        if self.isConnected:
            if "arg" in param and "tab" in param and "where" in param:
                sql = """SELECT """
                if len(param["arg"]) > 1:
                    sql += param["arg"][0]
                    for arg in param["arg"][1:]:
                        sql += "," + arg
                else:
                    sql += param["arg"][0]
                sql += " FROM " + param["tab"]
                if len(param["where"]) > 0:
                    sql += " WHERE "
                    if len(param) > 1:
                        sql += (
                            param["where"][0][0]
                            + " "
                            + param["where"][0][1]
                            + " "
                            + param["where"][0][2]
                        )
                        for cond in param["where"][1:]:
                            sql += " AND " + cond[0] + " " + cond[1] + " " + cond[2]
                    else:
                        sql += (
                            param["where"][0][0]
                            + " "
                            + param["where"][0][1]
                            + " "
                            + param["where"][0][2]
                        )
                try:
                    self.cursor.execute(sql)
                except mysql.connector.errors.ProgrammingError:
                    return ["[DataBase][SQL Error] SQL Error : " + sql, None]
            else:
                return ["[DataBase][Argument Error] Param error , need 3 param !", None]

            rows = self.cursor.fetchall()
            tt = []
            for row in rows:
                tt.append(row)

            return [
                "[DataBase][Succes]" + sql + " done " + str(len(tt)) + "row(s) found",
                tt,
            ]
        else:
            return ["[DataBase][Select Error] Connection not etablished", None]

    def quit(self):
        self.isConnected = False
        self.conn.close()
        return "[DataBase] Connection closed"


if __name__ == "__main__":
    db = dbManager("config.lg")
    statu1 = db.loadConfig()
    statu2 = db.connectDB()
    param = {
        "arg": ["name", "money"],
        "tab": "users",
        "where": [["bank", ">=", "3000"]],
    }
    res = db.getFromOneTable(**param)
    print(res[0])
    param = {
        "arg": ["name", "money", "bank"],
        "tab": "users",
        "where": [["bank", ">=", "3000"], ["bank", "<=", "90000"]],
    }
    res = db.getFromOneTable(**param)
    print(res[0])
    param = {"arg": ["*"], "tab": "users", "where": []}
    res = db.getFromOneTable(**param)
    print(res[0])
