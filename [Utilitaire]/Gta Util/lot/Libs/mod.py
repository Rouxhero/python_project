# Made By rouxhero
import time
from random import choice
from tkinter import *
from tkinter.filedialog import *
from os import system as shell


class ListeJ:
    def __init__(self):
        self.end = False
        shell("cls")
        self.main = Tk()
        self.main.title("Lot")
        self.Frame1 = LabelFrame(self.main, text="Saisi manuelle", relief=GROOVE)
        self.Fbut = Frame(self.Frame1)
        self.liste_joueur = []
        self.dico_JouerV = {}
        self.t = time.localtime()[:]
        self.houre = "(" + str(self.t[3]) + ":" + str(self.t[4]) + ")"
        self.date = str(self.t[2]) + "-" + str(self.t[1]) + "-" + str(self.t[0])
        self.winner = ""
        print("Watting player :")

    def add_Par(self):
        """
        liste : [name,nbvote]
        """
        self.nameP = self.name.get()
        self.nb = int(self.vote.get())
        self.dico_JouerV[self.nameP] = self.nb
        for i in range(self.nb):
            self.liste_joueur.append(self.nameP)
        print("-> Joueur " + self.nameP + " ajouté " + str(self.nb) + " Fois !")

    def add_ParW(self, nam, nbv):
        """
        liste : [name,nbvote]
        """
        self.nameP = nam
        self.nb = nbv
        self.dico_JouerV[self.nameP] = self.nb
        for i in range(self.nb):
            self.liste_joueur.append(self.nameP)
        print("-> Joueur " + self.nameP + " ajouté " + str(self.nb) + " Fois !")

    def SaveP(self):
        text = ""
        for player in self.dico_JouerV:
            if player == self.winner:
                text += (
                    "-> "
                    + player
                    + " a voté :"
                    + str(self.dico_JouerV[player])
                    + " fois. Et a gagné !\n"
                )
            else:
                text += (
                    "-> "
                    + player
                    + " a voté :"
                    + str(self.dico_JouerV[player])
                    + " fois.\n"
                )
        open("../History/" + self.date + ".txt", "a").write(text)
        text = (
            "-> "
            + self.winner
            + " a gagné le "
            + self.date
            + self.houre
            + " avec :"
            + str(self.dico_JouerV[self.winner])
            + "Votes\n"
        )
        open("../ListeGagnant.txt", "a").write(text)

    def tireG(self):
        self.bb.destroy()
        self.winner = choice(self.liste_joueur)
        print(
            "Le gagnant est "
            + self.winner
            + " avec "
            + str(self.dico_JouerV[self.winner])
            + " Votes !"
        )
        Label(self.Frame2, text="Winner is " + self.winner).pack()
        Button(self.Frame2, text="Quitter", command=self.quit).pack()
        self.SaveP()

    def Done(self):
        self.Frame1.destroy()
        self.LoadF.destroy()
        self.Frame2 = Frame(self.main)
        self.AfficheP = LabelFrame(self.main, text="Resumé des participant :")
        x = 1
        y = 1
        for i in self.dico_JouerV:
            Label(self.AfficheP, text=i, borderwidth=1, relief=GROOVE).grid(
                row=y, column=x, padx=5
            )
            x += 1
            if x > 3:
                x = 1
                y += 1

        self.bb = Button(self.Frame2, text="Get winner !", command=self.tireG)
        self.bb.pack()
        self.AfficheP.pack()
        self.Frame2.pack()

    def Load(self):
        filename = askopenfilename(
            title="Ouvrir le Fichier ", filetypes=[("txt files", ".txt")]
        )
        fichier = open(filename, "r").read()
        listTmp = fichier.split(";")
        for item in listTmp:
            if len(item) > 0:
                if item[0] != "#":
                    InfoJ = item.split("-")
                    nameP = InfoJ[0][1:]
                    nbvote = int(InfoJ[1])
                    self.add_ParW(nameP, nbvote)
        print("All player Added !")
        self.Done()

    def quit(self):
        print("Bye !")
        self.main.destroy()
        self.end = True
        shell('pause')

    def Start(self):
        self.LoadF = LabelFrame(self.main, text="ou alors :", width=30)
        Label(self.Frame1, text="Entrer le nom du joueur").pack(padx=5)
        self.name = Entry(self.Frame1, width=30)
        self.name.pack()
        Label(self.Frame1, text="Entrer le nombre de vote").pack(padx=5)
        self.vote = Entry(self.Frame1, width=30)
        self.vote.pack()
        Button(self.Fbut, text="Ajouter le joueur", command=self.add_Par).pack(
            side=LEFT, padx=15
        )
        Button(self.Fbut, text="Terminé !", command=self.Done).pack(side=RIGHT)
        Button(self.LoadF, text="Charger un fichier", command=self.Load).pack(
            side=RIGHT, padx=30, pady=10
        )
        self.Frame1.pack(padx=5, pady=5)
        self.LoadF.pack(side=BOTTOM, padx=15, pady=5)
        self.Fbut.pack(side=BOTTOM, padx=15, pady=5)
