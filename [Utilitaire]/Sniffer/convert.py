from math import floor
listeTerm = ["","K","M","G","T"]
listeCov = [8,1024,1048576,1073741824,1099511627776]
def convert(chiffre):
	chiffreS = abs(chiffre)
	i = 1
	ll = 0
	while floor(chiffre/i)>=1000:
		i *= 1000
		ll+=1
	return str(round(chiffreS/listeCov[ll],2))+listeTerm[ll]

def unconvert(strs):
	ind = strs[-1]
	dd = strs[:-1]
	try :
		listeTerm.index(ind)
	except ValueError:
		dd = strs
		ind = ""
	if len(strs.split('.'))>1 :
		chiffre = float(dd)
	
	else:
		chiffre = int(dd)
	return chiffre*listeCov[listeTerm.index(ind)]


print(convert(unconvert("1.55G")))
print(convert(unconvert("15")))