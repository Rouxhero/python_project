from Fetcher2 import Fetcher
from tkinter.filedialog import *
import tkinter as tk
from convert import convert,unconvert
import os

bot = Fetcher()
bot.load()


def helps():
    print(
        """
--------Help----------

Command List :
- load : load Folder
- prepare : prepare Condition for run
- run : Launch Sniffer in files
- cd : Changer Directories and reLoad
- clearScreen : clear console
- clear : clear bot cache
- status : show bot Status
- save : write all found files in save.txt
- date : change minDate of change
- double : fetch all double or multiple folder
- fetchFile : search specific file (Ex : main.py)
- preview : show files founded
- showDouble : show Double Dir
- maxChild : change value of bot 
- dirName : change value of bot
- showDir ; show dir info of cache file
- size : show size of current directories
- fetchSize : fetchFile from size
- quit : close Bot

        """
    )


def changeDir():
    dirs = askdirectory()
    bot.setPathFetch(dirs)
    bot.load()


def clears():
    os.system("cls")


def status():
    txt = ""
    for dirs in bot.dirOption["Ignore"]:
        txt += dirs + " | "
    print(
        """
Bot Status :

- Ready to Sniff : {}

- Working path : {}
- Directories Load : {}
- Dirs name : {}
- max Child : {}
- date mini : {}
- dir key : {}
- Sniffer Extension : {}
- Sniffer Key Word : {}
- Ignored Directories : {}
- file in cache : {}

        """.format(
            bot.checkF is not None,
            bot.saveOption["Path"],
            str(len(bot.allDir)),
            bot.dirOption["name"],
            bot.dirOption["MaxChild"],
            bot.dirOption["Date"],
            bot.dirOption["Keys"],
            bot.fileOption["Extension"],
            bot.fileOption["Key"],
            txt,
            str(bot.cache),
        )
    )


def fetchFiles():
    file = input("File : ")
    temp = file.split(".")
    bot.setFileExtension("." + temp[1])
    bot.setKeyWord(temp[0])
    bot.checkF = None


def shows():
    root = tk.Tk()
    scrollbar = tk.Scrollbar(root)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
    data = tk.Listbox(root, yscrollcommand=scrollbar.set, width=350)
    for result in bot.dataResult:
        txt = ""
        for path in bot.dataResult[result]:
            txt += path.path + "|"
        data.insert(
            tk.END,
            result
            + " -> ["
            + str(len(bot.dataResult[result]))
            + "] files : "
            + txt,
        )
    data.pack(side=tk.LEFT, fill=tk.BOTH)
    scrollbar.config(command=data.yview)
    root.mainloop()


def showDir():
    root = tk.Tk()
    scrollbar = tk.Scrollbar(root)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
    data = tk.Listbox(root, yscrollcommand=scrollbar.set, width=350)
    for result in bot.dataResult:
        for path in bot.dataResult[result]:
            size = bot.calculateSize(path.path)
            data.insert(tk.END, result + " -> " + path.path +" : " + convert(size))
    data.pack(side=tk.LEFT, fill=tk.BOTH)
    scrollbar.config(command=data.yview)
    root.mainloop()


def showDouble():
    root = tk.Tk()
    scrollbar = tk.Scrollbar(root)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
    data = tk.Listbox(root, yscrollcommand=scrollbar.set, width=350)
    for folder in bot.double:
        txt = ""
        for fl in bot.double[folder]:
            txt += "|" + fl.path
        data.insert(tk.END, folder.path + " = " + txt)
    data.pack(side=tk.LEFT, fill=tk.BOTH)
    scrollbar.config(command=data.yview)
    root.mainloop()

def showMaxSize():
    print("Type size and the prefix [|K|M|G|T]")
    size = unconvert(input("Size : "))
    root = tk.Tk()
    scrollbar = tk.Scrollbar(root)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
    data = tk.Listbox(root, yscrollcommand=scrollbar.set, width=350)
    datas = bot.getFileFromSize(size)
    for key in datas:
        data.insert(tk.END, key + " : " + convert(datas[key]))
    data.pack(side=tk.LEFT, fill=tk.BOTH)
    scrollbar.config(command=data.yview)
    root.mainloop()



def showSize():
    root = tk.Tk()
    scrollbar = tk.Scrollbar(root)
    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
    data = tk.Listbox(root, yscrollcommand=scrollbar.set, width=350)
    if bot.dirOption["MaxChild"]<=3 and bot.dirOption["MaxChild"]>0:
        for folder in bot.allDir:
          size = bot.calculateSize(folder.path)
          data.insert(tk.END, folder.path + " -> " + convert(size))
    else :
        size = bot.calculateSize(bot.saveOption["Path"])
        data.insert(tk.END, bot.saveOption["Path"] + " -> " + convert(size))
    data.pack(side=tk.LEFT, fill=tk.BOTH)
    scrollbar.config(command=data.yview)
    root.mainloop()




def changeDate():
    print("input date detail, to delete date arg , put -1 at all")
    day = int(input("Day : "))
    month = int(input("Month : "))
    year = int(input("Years : "))
    bot.setDate(day, month, year)
    bot.checkF = None


def changeName():
    name = input("dir Name : ")
    bot.setName(name)
    bot.checkF = None


def maxChildChange():
    name = int(input("Max Child [-1 : remove]: "))
    bot.setMaxChild(name)
    bot.checkF = None


def setKeyDir():
    name = input("dir Name : ")
    bot.setContain(name)
    bot.checkF = None


dicCommand = {
    "load": bot.load,
    "prepare": bot.prepareFile,
    "run": bot.launchSniffer,
    "cd": changeDir,
    "help": helps,
    "clearScreen": clears,
    "clear": bot.cleanResult,
    "status": status,
    "save": bot.save,
    "fetchFile": fetchFiles,
    "date": changeDate,
    "preview": shows,
    "dirName": changeName,
    "maxChild": maxChildChange,
    "dirKey": setKeyDir,
    "showDir": showDir,
    "size": showSize,
    "fetchSize":showMaxSize,
    "showDouble": showDouble,
    "quit": exit,
}

clears()
print(
    """
##########################
#      Create by         #
#                        #
#        Rouxhero        #
##########################

------Files Fetcher-------

-> type : help for help
        """
)

userChoice = "load"

while userChoice != "quit":
    userChoice = input("Fetcher$>>")
    allChoice = userChoice.split(";")
    for userChoice in allChoice:
        userChoice = userChoice.rstrip()
        if userChoice in dicCommand:
            dicCommand[userChoice]()
        else:
            print(userChoice + " :Unknown command")
