from math import floor
listeTerm = ["","K","M","G","T"]
listeCov = [8,1024,1048576,1073741824,1099511627776]
def convertStr(chiffre):
	chiffreS = abs(chiffre)
	i = 1
	ll = 0
	while floor(chiffre/i)>=1000:
		i *= 1000
		ll+=1
	return str(round(chiffreS/i,2))+listeTerm[ll]

def convertToBit(chiffre):
	chiffreS = abs(chiffre)
	i = 1
	ll = 0
	while floor(chiffre/i)>=1000:
		i *= 1000
		ll+=1
	return str(round(chiffreS/listeCov[ll],2))+listeTerm[ll]

def unconvertToDes(bits):
	ind = bits[-1]
	dd = bits[:-1]
	try :
		listeTerm.index(ind)
	except ValueError:
		dd = bits
		ind = ""
	if len(bits.split('.'))>1 :
		chiffre = float(dd)
	
	else:
		chiffre = int(dd)
	return chiffre*listeCov[listeTerm.index(ind)]


if __name__ == "__main__":
	octets = 10
	print(convertToBit(octets))