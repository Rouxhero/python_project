##Function to unpack data

def traduit(bins):
	if len(bins)==2:
		return int(bins)
	elif len(bins)==4:
		return [bool(bins[:2]),bool(bins[2:])]
def unpack(data):
	"""
	:param data : txt
	"""
	brut = data.split("|")
	for line in brut:
		print(traduit(line))


test = "01 |1100|"
unpack(test)