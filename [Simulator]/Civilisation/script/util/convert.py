from math import floor
listeTerm = ["","K","M","G","T","B","aa","ab","ac","ad","ae","af","ba","bb","bc","bd","be","bf","ca"]
def convert(chiffre):
	chiffreS = abs(chiffre)
	i = 1
	ll = 0
	while floor(chiffre/i)>=1000:
		i *= 1000
		ll+=1
	return str(floor(chiffreS/i))+listeTerm[ll]
