#Class log for consol

class Logs:
	def __init__(self,user,system):
		self.history = []
		self.user = user+'@'+system

	def addCmd(self,cmd):
		self.history.append("$"+self.user+"#>> "+cmd)
	def addReturn(self,cmd):
		self.history.append("-> "+cmd)
	def addLog(self,log):
		self.history.append("$"+self.user+" : "+log)
	def clearCmd(self):
		self.history = []
	def show(self):
		for line in self.history:
			print(line)

