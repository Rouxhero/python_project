#All utile function


def maxPerLine(table):
	listeMax = list(range(len(table[0])))
	colum = []
	for line in range(len(table[0])):
		linee = []
		for x in table:
			linee.append(str(x[line]))
			listeMax[line] = max(listeMax[line],len(str(x[line])))
		colum.append(linee)
	return listeMax,colum

def table_to_str(tab):
	listeMax , colum = maxPerLine(tab)
	tableau = ["+"]
	for i in listeMax:
		tableau[0]+= "="*i+"+"
	#Head
	tableau.append("|")
	tableau.append("+")
	for i in range(len(colum)):
		tableau[1] += colum[i][0] + " "*(listeMax[i]-len(colum[i][0]))+"|"
		tableau[2] += "="*listeMax[i]+"+"
	if len(tab)>1:
		#Body first line
		for i in range(1,len(colum[0])):
			tableau.append("|"+colum[0][i]+" "*(listeMax[0]-len(colum[0][i]))+"|")
		for i in range(1,len(colum)):
			for ii in range(1,len(colum[i])):
				tableau[2+ii] += colum[i][ii]+" "*(listeMax[i]-len(colum[i][ii]))+"|"

		#end
		tableau.append('+')
		for i in listeMax:
			tableau[len(tableau)-1]+= "-"*i+"+"
	return tableau
def afficheTableau(tableau):
	tab = table_to_str(tableau)
	if tab != None :
		for i in tab:
			print(i)

liste = [("N°","a","b","c","Bureau","salon1","salon2"),
(0,0,0,0,0,0,0),
(1,0,0,1,1,1,0),
(2,0,1,0,0,0,1),
(3,0,1,1,1,0,0),
(4,1,0,0,0,0,0),
(5,1,0,1,0,1,1),
(6,1,1,0,1,0,1),
(7,1,1,1,1,0,0)]
afficheTableau(liste)