import sys
import lxml
from random import randint as r
from random import choice
from vispy import app, scene, geometry
import numpy as np
from log import console
from planete import planete



alph = "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN"
nb = "0123456789"




if __name__ == "__main__" :
	cons = console()
	if len(sys.argv) != 2 :
		raise Exception("argument Error, synthax : generate.py [nameofplanete]")
	

	name = sys.argv[1]
	cons.addPerma('Generating Planete : '+name)
	cons.show()
	diametre = r(1600,200000)
	cons.addPerma('Planete size : '+str(diametre)+"km")
	cons.show()
	
	
	nbOfMoon = r(1,5)
	cons.addPerma('Planete moon : '+str(nbOfMoon))
	cons.show()
	moon = {}
	for x in range(nbOfMoon):
		nameMoon = name+"_"+choice(alph)+choice(nb)+choice(nb)
		diametreMoon = r(int(diametre*0.01),int(diametre*0.05))
		isViable = choice([True,False])
		moon[nameMoon] = [diametreMoon,isViable]
		cons.addPerma('Moon : {} , size : {}'.format(nameMoon,diametreMoon))
		cons.show()
	planetes = planete(name,moon,diametre,cons)
	planetes.show()
