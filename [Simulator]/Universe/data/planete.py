from color import *
from vispy import app, scene, geometry
import numpy as np # utilisé pour les listes d'arêtes et de faces
from random import randint
from random import choice


def translate(obj, vect):
	t = scene.transforms.MatrixTransform()
	t.translate(vect)
	obj.transform = t

class planete :

	def __init__(self,name,dataMoon,diametre,cons,new=True):
		self.cons = cons
		print("Generating Planete: ")
		if new :
			self.pdata,color = self.generatePlanete(diametre,int(diametre/1000))
			open(name+".clr","w").write(self.save(color))
		else :
			pdatatxt = open(name+".clr","r").read()
			radius = diametre/1000
			if radius >5:
				radius =5
			self.pdata = geometry.create_sphere(diametre, 32,  radius=radius)
			color = self.extract(pdatatxt)
			self.pdata.set_face_colors(color,indexed=None)
		self.cons.show()
		self.cons.addPerma('Generating Planete: Done')
		self.canvas = scene.SceneCanvas(title="planet", size=(800,600), keys='interactive')
		self.view = self.canvas.central_widget.add_view()
		self.view.camera = "turntable"
		self.view.camera.distance=int(diametre/200)
		self.view.bgcolor = '#00041a'
		mesh = scene.visuals.Mesh(meshdata=self.pdata, shading='flat')
		# meshMoon = scene.visuals.Mesh(meshdata=mdataMoon,color="grey",shading='flat')
		# translate(meshMoon,(randint(0,3),randint(0,3),9))
		self.view.add(mesh)
		for moon in dataMoon:
			print("Generating moon {}: ".format(moon))
			moons = self.generateMoon(dataMoon[moon],diametre)
			self.view.add(moons)
			self.cons.addPerma("Generating moon {}: Done".format(moon))
			self.cons.show()
   		
   		# self.view.add(meshMoon)

	def generateMoon(self,data,diametre):
		radius = int(data[0]/1000)
		mins = int(diametre/1000)
		get = lambda:randint(mins,mins+2)
		mdataMoon,color = self.generatePlanete(data[0],radius,True)
		meshMoon = scene.visuals.Mesh(meshdata=mdataMoon,color=choice(MOON),shading='flat')
		coord = (choice([-get(),get()]),choice([-get(),get()]),choice([-get(),get()]))
		print(coord)
		translate(meshMoon,coord)
		return meshMoon
	def save(self,data):
   		txt = ""
   		for x in data:
   			for xx in x:
   				txt += str(xx) + "|"
   			txt += ";"
   		return txt
	def extract(self,data):
		liste = []
		da  = data.split(';')
		for daa in da:
			under = []
			dataa = daa.split('|')
			if len(dataa)==5:
				for i in dataa[:3]:
					if len(i) != 1 :
						under.append(float(i))
					else :
						under.append(int(i))
				liste.append(under)
		return liste
	def show(self):

		self.canvas.show()
		app.run()

	def generatePlanete(self,diametre,raduis,isMoon = False):
		indexCAse = diametre
		radius = raduis
		if not isMoon:
			if diametre > 5000:
				indexCAse = 5000
			if radius >5:
				radius =5
		else :
			if diametre >1600:
				indexCAse = 1600
			if radius > 3:
				radius = 3

		mdata = geometry.create_sphere(indexCAse, 32,  radius=raduis)
		nbFace = int(mdata.n_faces*0.33)
		color = []
		x = mdata.get_vertex_faces()
		water = choice(GROUND1)
		for face in range(x[-1][-1]+1):
			color.append(water)
		indexCont = []
		allTile = []
		for i in range(10):
			index = randint(200,len(color)//1.7)
			indexCont.append(index)
			allTile.append(index)

		outContLeft = {}
		outContDown = {}
		outContUp = {}
		for cont in indexCont:
			outContLeft[cont] = [cont,]
			outContDown[cont] = [cont,]
			outContUp[cont] = [cont,]

		def addToOut(tile):
			outContLeft[cont].append(tile)
			outContDown[cont].append(tile)
			outContUp[cont].append(tile)



		while len(allTile)<nbFace:
			index = randint(1,10)
			cont = choice(indexCont)
			if index <= 2:
				selected = choice(outContLeft[cont])
				allTile.append(selected+1)
				outContLeft[cont].remove(selected)
				addToOut(selected+1)
			elif index>2 and index<=6 :
				selected = choice(outContDown[cont])
				allTile.append(selected+indexCAse)
				outContDown[cont].remove(selected)
				addToOut(selected+indexCAse)
			else:
				selected = choice(outContUp[cont])
				allTile.append(selected-indexCAse)
				outContUp[cont].remove(selected)
				addToOut(selected-indexCAse)

			
		#end
		ground =  choice(GROUND2)
		for index in allTile:
		   	 if index <len(color)-1:
		   	 	color[index]=ground
		for i in x[0]:
			color[i] = [1,1,1,1]
			color[-i] = [1,1,1,1]
		mdata.set_face_colors(color, indexed= None)
		return mdata,color

if __name__ == '__main__':
	moon = {"lune1":[800,15],"lune2":[1600,15]}
	planetes = planete("E638",moon,20800)
	planetes.show()