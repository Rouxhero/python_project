from vispy import app, scene, geometry
import numpy as np # utilisé pour les listes d'arêtes et de faces
from random import randint
from random import choice


def calculeNb(allCont):
    nb = 0
    seen = []
    for cont in allCont:
        for index in cont:
            if not index in seen :
                seen.append(index)
                nb+=1
            else :
                cont.remove(index)
    return nb

def translate(obj, vect):
    t = scene.transforms.MatrixTransform()
    t.translate(vect)
    obj.transform = t

def create_scene(view):
    # Création de la sphère
    mdata = geometry.create_sphere(2800, 32,  radius=5)
    mdataMoon = geometry.create_sphere(32, 32,  radius=1)
    color = []
    x = mdata.get_vertex_faces()

    print(mdata.n_faces)
    print(mdata.n_vertices)
    # tt = ""
    # for dd in  x:
    #      tt+=str(dd)+"\n"
    # open("ttt.txt","w").write(tt)


    # y = {}
    # y[0] = mdata.get_vertex_faces()[0]
    # count = 1
    # index = -1 
    # data  =  [mdata.get_vertex_faces()[1]]
    # for face in range(2,len(x)):
    #     if index != x[face][0]-1:
    #         y[count] = data
    #         count += 1
    #         data = []
    #     data.append(x[face])
    #     index = x[face][0]   
        
    # y[count] = data
    # print(len(y),len(x))
    # tt = ""
    # for dd in y:
    #     for ddd in y[dd]:
    #         tt+=str(ddd)+"\n"
    #     tt+= "\n"
    # open("tt.txt","w").write(tt)
    # print(len(x))
    for face in range(x[-1][-1]+1):
        color.append([0,0.5,0.8,1])




    # for face in y[30]:
    #     for xs in face:
    #         color[xs] = [1,1,1,1]
    

    # indexCont = []
    # for i in range(10):
    #     index = randint(200,len(color)//1.5)
    #     indexCont.append([index,index])
    #     color[index]= [1,1,1,1]


    # while calculeNb(indexCont)<int(mdata.n_faces*0.33):
    #     print(calculeNb(indexCont) ,"generated on : ",int(mdata.n_faces*0.33))
    #     index = randint(1,10)
    #     cont = choice(indexCont)
    #     if index <= 2:
    #         cont.append(cont[-1]+1)
    #     elif index>2 and index<=6 :
    #         i = randint(0,len(cont)-1)
    #         cpt = 0
    #         while cont[i]+30 > len(color)-200:
    #             cpt += 1
    #             if cpt >200:
    #                 break
    #             i = randint(0,len(cont)-1)
    #         cont.append(cont[i]+30)
    #     else:
    #         i = randint(0,len(cont)-1)
    #         cpt = 0
    #         while cont[i]-30 <200:
    #             cpt += 1
    #             if cpt >200:
    #                 break
    #             i = randint(0,len(cont)-1)
    #         cont.append(cont[i]-30)

    # for cont in indexCont:
    #     for index in cont:
    #         if index <len(color)-1:
    #             color[index]= [0,0.8,0.2,1]

    color[3] = [1,1,1,1]
    color[67] = [1,1,1,1]
    # for i in x[0]:
    #     color[i] = [1,1,1,1]
    #     color[-i] = [1,1,1,1]

    mdata.set_face_colors(color, indexed= None)
    mesh = scene.visuals.Mesh(meshdata=mdata, shading='flat')
    meshMoon = scene.visuals.Mesh(meshdata=mdataMoon,color="grey",shading='flat')
    translate(meshMoon,(randint(0,3),randint(0,3),9))
    view.add(mesh)
    view.add(meshMoon)


# Création du canvas
canvas = scene.SceneCanvas(title="planet", size=(800,600), keys='interactive')
# Ajout de la vue dans le canvas (nous aurons toujours une seule vue)
view = canvas.central_widget.add_view()
# Réglage de la caméra
view.camera = "turntable"
view.camera.distance=25
create_scene(view)
#view.update()
canvas.show()
app.run()