from lxml import etree

open("resources.xml","wb").write(b'<?xml version="1.0" encoding="UTF-8"?>\n')
users = etree.Element("root")

def makeId(nb):
	nb = str(nb)
	oo = 4-len(nb)
	return "0"*oo+nb
users_data = {
"wood":{"units":"","breakable":"true","maxP":5},
"oxigene":{"units":"L","breakable":"true","maxP":5},
"gas":{"units":"L","breakable":"true","maxP":5},
"granite":{"units":"","breakable":"true","maxP":5},
"diorite":{"units":"","breakable":"true","maxP":5},
"andesite":{"units":"","breakable":"true","maxP":5},
"stone":{"units":"","breakable":"true","maxP":5},
"grass":{"units":"","breakable":"true","maxP":5},
"dirt":{"units":"","breakable":"true","maxP":5},
}

count = 0
for user_data in users_data:
    head = etree.SubElement(users, "resources")
    head.set("data-id", makeId(count))
    nom = etree.SubElement(head, "name")
    nom.text = user_data
    for key in users_data[user_data] :
    	nom = etree.SubElement(head, key)
    	nom.text = str(users_data[user_data][key])
    count+=1;

open("resources.xml","ab").write(etree.tostring(users, pretty_print=True))
print("end")