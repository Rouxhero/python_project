# class for ground
from tile.var import *
from random import choice
class ground :

	def __init__(self,x,y,data):
		self.pos = (x,y)
		self.data = data
		self.status = data['statu']
		self.image = self.data['img'][0]
		self.otherStatus = [None,None,None,None]
		self.animation = {"status":0,"cpt":0}
	def updateStatu(self):
		if self.data['active']:
			code = ""
			for tile in self.otherStatus:
				if tile != None:
					code += codePath[tile.status]
				else:
					code += "0"
			if code in satusPath:
				self.image = self.data['img'][satusPath[code]]
			else :
				codes = code.split('2')
				code = "1".join(codes)
				self.image = self.data['img'][satusPath[code]]
		else :
			self.animation['cpt'] +=1
			if self.animation['cpt'] >= 15:
				self.image = self.data['img'][self.animation['status']]
				self.animation["status"] = (self.animation["status"]+1)%(len(self.data['img']))
				self.animation['cpt'] = 0