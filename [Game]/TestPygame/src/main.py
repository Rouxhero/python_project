from load import *
from tile.ground import *
import pygame
from pygame.locals import *
from random import randint
gameLoader = LoadGameFile("../data/data.xml")
allData = gameLoader.data
# maps ={
# 	"type":"g",
# 	"data":[
# 		["002","002","002","001","001"],
# 		["002","002","002","002","001"],
# 		["001","001","001","002","001"],
# 		["001","001","001","002","001"],
# 		["001","001","001","002","002"],
# 		]
# 	}
maps = {
	"type":"g",
	"data": []
}
for xx in range(10):
	maps['data'].append( list("00"+str(randint(1,3)) for x in range(0,15))) 
mapsLoaded = []
for y in range(len(maps["data"])):
	temp = []
	for x in range(len(maps["data"][y])):
		temp.append(ground(x,y,allData[maps["type"]][maps["data"][y][x]]))
	mapsLoaded.append(temp)

for y in range(len(mapsLoaded)):
		for x in range(len(mapsLoaded[y])):
			if y-1>-1:
				mapsLoaded[y][x].otherStatus[3] = mapsLoaded[y-1][x]
			if x-1>-1:
				mapsLoaded[y][x].otherStatus[1] = mapsLoaded[y][x-1]	
			if x+1<len(mapsLoaded[y]):
				mapsLoaded[y][x].otherStatus[2] = mapsLoaded[y][x+1]
			if y+1<len(mapsLoaded):
				mapsLoaded[y][x].otherStatus[0] = mapsLoaded[y+1][x]


def showMaps():
	for line in mapsLoaded:
		for theTile in line:
			theTile.updateStatu()
			surface.blit(theTile.image,(theTile.pos[0]*48,theTile.pos[1]*48))

pygame.init()
pygame.font.init()
clock = clock = pygame.time.Clock()
surface = pygame.display.set_mode((800, 600))

pygame.key.set_repeat(400, 30)

continu = True
while continu:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			continu = True
			pygame.quit()
			quit()
		if event.type == pygame.MOUSEBUTTONDOWN:
			x,y = pygame.mouse.get_pos()
			x,y=x//48,y//48
	showMaps()
	clock.tick(15)
	pygame.display.update()