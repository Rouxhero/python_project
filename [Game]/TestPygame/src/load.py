# coding: utf-8
# class to load option
from lxml import etree
from Error import Erreur as alert
import pygame
from pygame.locals import *
booleanDic = {"true":True,"false":False}
typeDico = {
		"g":{"type":"ground","img":[],"walk":None,"statu":"","active":""},
		"o":{"type":"objet","img":[],"solid":None,"script":""}		
}


class LoadGameFile:

	def __init__(self,path):
		self.tree =  etree.parse(path)
		self.data = {"g":{},"o":{}}
		self.load()

	def load(self):
		for tile in self.tree.xpath("/root/tile"):
			self.__addTile(tile)


	def __convert(self,line):
		newLine = None
		if line in booleanDic :
				newLine = booleanDic[line]
		else :
			try :
				newLine = int(line)
			except ValueError :
				newLine = line
		return newLine;

	def __addTile(self,tile):
		theDict = dict(typeDico[tile.get('id')[0]])
		theDict['img'] = list()
		theDict["id"] = tile.get('id')
		for data in tile:
			if data.tag in theDict:
				if data.tag !="img":
					theDict[data.tag] = self.__convert(data.text)
				else :
					theDict[data.tag].append(self.__convert(data.text))
			else :
				raise alert.GameFileCorruptError(data.tag+" Error Unknown key !")
		imgLoaded= []
		for img in theDict["img"]:
			imgLoaded.append(pygame.image.load("../data/img/"+theDict["type"]+"/"+img))
		theDict["img"] = imgLoaded
		self.data[tile.get('id')[0]][tile.get('id')[1:]] = theDict

if __name__ == "__main__":
	gameLoader = LoadGameFile("../data/data.xml")
	print(gameLoader.data)
