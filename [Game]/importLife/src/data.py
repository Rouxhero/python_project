# -*- coding: utf-8 -*-
# Class for game Data
try:
    from lxml import etree
except ImportError:
    import os

    os.system("pip3 install lxml")


class GameData:
    def __init__(self, dirPath):
        self.dirPath = dirPath
        self.clas = {"passif.xml": []}

    def loadData(self):
        for file in self.clas:
            tree = etree.parse(self.dirPath + file)
            for user in tree.xpath("/root/animal"):
                info = {}
                data = user.getchildren()
                ids = user.get("data-id")
                info["id"] = ids
                for item in data:
                    info[item.tag] = item.text
                self.clas[file].append(info)
        for creature in self.clas["passif.xml"]:
            print(creature)


if __name__ == "__main__":
    aa = GameData("../data/")
    aa.loadData()
