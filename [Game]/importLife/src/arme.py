#Definition
"""
degats : degats de l'armes
miss : % de rater le coups
critique : % de faire critique
max : coup critique
pref : commun

"""
stick = {
	
	"Degats":3,
	"miss":25,
	"critique":6,
	"max":9,
	"Pref":"commun"
}
sword = {
	
	"Degats":16,
	"miss":35,
	"critique":25,
	"max":28,
	"Pref":"Soldat"
}
mage_stick = {
	
	"Degats":10,
	"miss":25,
	"critique":45,
	"max":28,
	"Pref":"mage"
}