dark = {
    "backgroundFrame": "#32374b",
    "backgroundWindow": "#010821",
    "backgroundLabel": "#32374b",
    "ForegroundLabel": "#dbdbdb",
    "ForegroundTitle": "#980724",

    "Button": "#3971a5",
    "LabelFont": ("Helvetica", 20),
    "NewsFont": ("Helvetica", 12)
}
light = {
    "backgroundFrame": "#f4f4f4",
    "backgroundWindow": "#e7e7e7",
    "backgroundLabel": "#f4f4f4",
    "ForegroundLabel": "#55cdfb",
    "ForegroundTitle": "#010821",

    "Button": "#63b67d",
    "LabelFont": ("Helvetica", 20),
    "NewsFont": ("Helvetica", 12)
}



themeTab = {"dark": dark,"light":light}
