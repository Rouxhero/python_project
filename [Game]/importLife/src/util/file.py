# -*- coding: utf-8 -*-

# Class to create SaveApp

class Files:
    def __init__(self, name, bools):
        if bools:
            self.name = "../data/" + name
        else:
            self.name = name
        self.file = """#Files Generator\n"""
        self.option = {}

    def init(self):
        try:
            self.option = self.loadAllOption()
        except FileNotFoundError:
            with open(self.name, "w") as f:
                f.write(self.file)
                self.option = {}
            return "[File][Error] : Files doesn't exist! => File created"
        return "[File][Succes] Files " + self.name + " open !"
        
    def addOption(self, name, value):
        self.option[name] = value
        return "[File][Succes] Option added !"
    def reloadOption(self,dico):
        self.option = dico

    def loadAllOption(self):
        option = {}
        key = None
        lines = open(self.name, "r").read().split("\n")
        for line in lines:
            if len(line) > 1:
                if line[0] != "#":
                    if line[0] == "-":
                        key = line[1:]
                    else:
                        if key != None:
                            line = self.uncode(line)
                            option[key] = line
                            key = None
        return option

    def getAllOption(self):
        return self.option

    def uncode(self, line):
        booleanDic = {
            "True": True,
            "true": True,
            "False": False,
            "false": False,
        }
        if line in booleanDic:
            return booleanDic[line]
        else:
            try:
                return int(line)
            except ValueError:
                return line

    def saveFiles(self):
        for key in self.option:
            self.file += "-"+key+"\n"+str(self.option[key])+"\n"
        with open(self.name, "w") as f:
            f.write(self.file)
        self.file = """#Files Generator\n"""
        return "[File][Succes] Config files saved at : " + self.name


if __name__ == "__main__":
    save = Files("E:/Desktop/importLife/save/Rouxhero.llg", False)
    save.init()
    save.addOption("Scale", "451x152")
    save.addOption("age", "20")
    save.addOption("FirstL", "true")
    save.addOption("DbConnected", "false")
    save.addOption("DbisConfig", "true")
    save.saveFiles()
    print(save.getAllOption())

