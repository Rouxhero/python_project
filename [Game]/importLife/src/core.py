# -*- coding: utf-8 -*-

from util.log import Logs
from util.file import Files
import data


class Core:
    def __init__(self, saveName):
        self.saveName = saveName
        self.logs = Logs("../log/")
        self.playerData = None
        self.saveFile = Files(self.saveName, False)
        self.logs.addLog(self.saveFile.init())
        self.data = data.GameData("../data/")

    def loadSave(self):
        self.playerData = self.saveFile.getAllOption()

    def createSave(self, info):
        self.saveFile.reloadOption(info)
        self.playerData = info


if __name__ == "__main__":
    body = Core("../save/Rouxhero.llg")
    body.loadSave()
    infoPlayer = {
        "Pseudo": "Rouxhero",
        "Name": "Ileo",
        "Lastname": "Ranger",
        "Class": "Mage",
        "Age": 20,
        "Vitality": 100,
    }
    body.createSave(infoPlayer)
    print(body.playerData)
    body.saveFile.saveFiles()
