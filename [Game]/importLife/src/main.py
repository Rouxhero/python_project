# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter.filedialog import *
from util.file import Files
from theme import *
from core import Core


def alert():
    print("ee")


class Application(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        # Liste des Frame
        self.files = Files("config.lg", True)
        self.files.init()
        self.option = self.files.getAllOption()
        self.option["Screen"] = (
            str(self.winfo_screenwidth()) + "x" + str(self.winfo_screenheight())
        )
        self.option["width"] = self.winfo_screenwidth()
        self.option["height"] = self.winfo_screenheight()
        self.option["themeVar"] = themeTab[self.option["theme"]]
        self.files.reloadOption(self.option)
        self.files.saveFiles()
        self.geometry(self.option["Screen"])
        self.bind("<F12>", self.toggleFullScreen)
        self.attributes("-fullscreen", self.option["fullscreen"])
        self.configure(bg=self.option["themeVar"]["backgroundWindow"])
        # Definition des Frames du menu
        self.main = tk.LabelFrame(
            self, text="Play", bg=self.option["themeVar"]["backgroundFrame"]
        )
        self.main.grid()
        self.info = tk.LabelFrame(
            self,
            text="News",
            bg=self.option["themeVar"]["backgroundFrame"],
        )
        # Definition du menu deroulant

        # self.setMenu()
        # Init de l'app
        self.showStart()

    # Key F12
    def toggleFullScreen(self, event):
        self.option["fullscreen"] = not self.option["fullscreen"]
        self.attributes("-fullscreen", self.option["fullscreen"])
        self.files.reloadOption(self.option)
        self.files.saveFiles()

    # Button
    def toggleFullScreenB(self):
        self.option["fullscreen"] = not self.option["fullscreen"]
        self.attributes("-fullscreen", self.option["fullscreen"])
        self.files.reloadOption(self.option)
        self.files.saveFiles()

    def loadSave(self):
        filename = askopenfilename(
            initialdir="../save/",
            title="Load Save",
            filetypes=[("llg file", ".llg"), ("all files", ".*")],
        )
        self.core = Core(filename)
        self.core.loadSave()
        self.launchGame()

    def createPlayer(self):
        self.main.destroy()
        self.main = tk.LabelFrame(
            self, text="New Player", bg=self.option["themeVar"]["backgroundFrame"]
        )
        self.main.grid()
        tk.Label(
            self.main,
            text="Name",
            bg=self.option["themeVar"]["backgroundLabel"],
            fg=self.option["themeVar"]["ForegroundLabel"],
        ).grid(row=0, column=0)
        tk.Label(
            self.main,
            text="LastName",
            bg=self.option["themeVar"]["backgroundLabel"],
            fg=self.option["themeVar"]["ForegroundLabel"],
        ).grid(row=0, column=2)
        tk.Label(
            self.main,
            text="Age",
            bg=self.option["themeVar"]["backgroundLabel"],
            fg=self.option["themeVar"]["ForegroundLabel"],
        ).grid(row=2, column=0)
        tk.Label(
            self.main,
            text="Save Name",
            bg=self.option["themeVar"]["backgroundLabel"],
            fg=self.option["themeVar"]["ForegroundLabel"],
        ).grid(row=2, column=2)
        tk.Label(
            self.main,
            text="Select Scenario ",
            bg=self.option["themeVar"]["backgroundLabel"],
            fg=self.option["themeVar"]["ForegroundLabel"],
        ).grid(row=4, column=1)
        tk.Label(
            self.main,
            text="Select sexe",
            bg=self.option["themeVar"]["backgroundLabel"],
            fg=self.option["themeVar"]["ForegroundLabel"],
        ).grid(row=6, column=1)
        tk.Label(
            self.main,
            text="Select class",
            bg=self.option["themeVar"]["backgroundLabel"],
            fg=self.option["themeVar"]["ForegroundLabel"],
        ).grid(row=8, column=1)
        self.ageSpin = tk.Spinbox(self.main, from_=18, to=500)
        self.ageSpin.grid(row=3, column=0, padx=5, pady=5)
        self.nameEntry = tk.Entry(self.main, width=30)
        self.lastNameEntry = tk.Entry(self.main, width=30)
        self.saveEntry = tk.Entry(self.main, width=30)
        self.nameEntry.grid(row=1, column=0, padx=5, pady=5)
        self.lastNameEntry.grid(row=1, column=2, padx=5, pady=5)
        self.saveEntry.grid(row=3, column=2, padx=5, pady=5)
        self.value = StringVar()
        self.sexe = StringVar()
        self.clas = StringVar()
        # For scenario choice
        tk.Radiobutton(
            self.main,
            text="Ileo",
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            variable=self.value,
            value=1,
        ).grid(row=5, column=0)
        tk.Radiobutton(
            self.main,
            text="Marcus",
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            variable=self.value,
            value=2,
        ).grid(row=5, column=1)
        # tk.Radiobutton(self.main, text="Coming soon",
        #     bg=self.option["themeVar"]["Button"],
        #     relief=tk.GROOVE,
        #     variable=self.value, value=3).grid(row=5, column=2, padx=5, pady=5)
        # For sexe choice
        tk.Radiobutton(
            self.main,
            text="Male",
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            variable=self.sexe,
            value=0,
        ).grid(row=7, column=0)
        tk.Radiobutton(
            self.main,
            text="Female",
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            variable=self.sexe,
            value=1,
        ).grid(row=7, column=1)
        tk.Radiobutton(
            self.main,
            text="Other",
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            variable=self.sexe,
            value=2,
        ).grid(row=7, column=2)
        # For class choice
        tk.Radiobutton(
            self.main,
            text="Mage",
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            variable=self.clas,
            value=0,
        ).grid(row=9, column=0)
        tk.Radiobutton(
            self.main,
            text="Soldat",
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            variable=self.clas,
            value=1,
        ).grid(row=9, column=1)
        tk.Radiobutton(
            self.main,
            text="Farmer",
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            variable=self.clas,
            value=2,
        ).grid(row=9, column=2)

        # Button
        tk.Button(
            self.main,
            text="Valider",
            command=self.createNewPlayer,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=10, column=2, padx=5, pady=5)
        tk.Button(
            self.main,
            text="Back",
            command=self.offLineMenu,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=10, column=0, padx=5, pady=5)
        self.main.place(in_=self, anchor="c", relx=0.5, rely=0.5)

    def createNewPlayer(self):
        age = self.ageSpin.get()
        name = self.nameEntry.get()
        lastN = self.lastNameEntry.get()
        save = self.saveEntry.get()
        scenario = self.value.get()
        sexe = ["Male", "Female", "Other"][int(self.sexe.get())]
        classPlayer = ["MAge", "Soldat", "Farmer"][int(self.clas.get())]
        data = {
            "Pseudo": save,
            "Name": name,
            "LastName": lastN,
            "Age": age,
            "sexe": sexe,
            "Class": classPlayer,
            "Scenario": scenario,
            "Vitality": 100,
        }
        print(data)

    def launchGame(self):
        pass

    def showStart(self):
        # Menu
        self.main.destroy()
        self.main = tk.LabelFrame(
            self, text="Play", bg=self.option["themeVar"]["backgroundFrame"]
        )
        self.main.grid()
        tk.Label(
            self.main,
            text="Import Life 2.0",
            bg=self.option["themeVar"]["backgroundLabel"],
            fg=self.option["themeVar"]["ForegroundTitle"],
            font=self.option["themeVar"]["LabelFont"],
        ).grid(row=0, column=2)
        tk.Button(
            self.main,
            text="Play Offline",
            command=self.offLineMenu,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=2, column=2, padx=5, pady=5)
        if not self.option["isOnline"]:
            tk.Button(
                self.main,
                text="Login",
                command=alert,
                bg=self.option["themeVar"]["Button"],
                relief=tk.GROOVE,
                width=30,
            ).grid(row=4, column=2, padx=5, pady=5)
        else:
            tk.Button(
                self.main,
                text="Play Online",
                command=alert,
                bg=self.option["themeVar"]["Button"],
                relief=tk.GROOVE,
                width=30,
            ).grid(row=4, column=2, padx=5, pady=5)
            tk.Label(
                self.main,
                text="",
                bg=self.option["themeVar"]["backgroundLabel"],
            ).grid(row=7, column=2, padx=5, pady=5)
            tk.Button(
                self.main,
                text="Account",
                command=alert,
                bg=self.option["themeVar"]["Button"],
                relief=tk.GROOVE,
                width=30,
            ).grid(row=6, column=2, padx=5, pady=5)
        tk.Button(
            self.main,
            text="Setting",
            command=self.showOption,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=8, column=2, padx=5, pady=5)
        # For space
        tk.Label(
            self.main,
            text="",
            bg=self.option["themeVar"]["backgroundLabel"],
        ).grid(row=1, column=2, padx=5, pady=5)
        tk.Label(
            self.main,
            text="",
            bg=self.option["themeVar"]["backgroundLabel"],
        ).grid(row=3, column=2, padx=5, pady=5)
        tk.Label(
            self.main,
            text="",
            bg=self.option["themeVar"]["backgroundLabel"],
        ).grid(row=5, column=2, padx=5, pady=5)
        self.main.pack(side=tk.LEFT, padx=10)
        # Info/News
        self.info.destroy()
        self.info = tk.LabelFrame(
            self,
            text="News",
            bg=self.option["themeVar"]["backgroundFrame"],
        )
        # Menu
        tk.Label(
            self.info,
            text=open("../data/new.lg", "r", encoding="utf_8").read(),
            bg=self.option["themeVar"]["backgroundLabel"],
            fg=self.option["themeVar"]["ForegroundLabel"],
            font=self.option["themeVar"]["NewsFont"],
            width=int(self.option["width"] * 0.50),
            height=self.option["height"],
        ).pack()
        self.info.pack(side=tk.RIGHT)

    def offLineMenu(self):
        self.main.destroy()
        self.info.destroy()
        self.main = tk.LabelFrame(
            self, text="Play Offline", bg=self.option["themeVar"]["backgroundFrame"]
        )
        self.main.grid()
        tk.Label(
            self.main,
            text="Play OffLine",
            bg=self.option["themeVar"]["backgroundLabel"],
            fg=self.option["themeVar"]["ForegroundTitle"],
            font=self.option["themeVar"]["LabelFont"],
        ).grid(row=0, column=2)
        tk.Button(
            self.main,
            text="New Player",
            command=self.createPlayer,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=2, column=2, padx=5, pady=5)
        tk.Button(
            self.main,
            text="Load Player",
            command=self.loadSave,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=4, column=2, padx=5, pady=5)
        tk.Button(
            self.main,
            text="Back",
            command=self.showStart,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=6, column=2, padx=5, pady=5)
        tk.Label(
            self.main,
            text="",
            bg=self.option["themeVar"]["backgroundLabel"],
        ).grid(row=1, column=2, padx=5, pady=5)
        tk.Label(
            self.main,
            text="",
            bg=self.option["themeVar"]["backgroundLabel"],
        ).grid(row=3, column=2, padx=5, pady=5)
        tk.Label(
            self.main,
            text="",
            bg=self.option["themeVar"]["backgroundLabel"],
        ).grid(row=5, column=2, padx=5, pady=5)
        self.main.place(in_=self, anchor="c", relx=0.5, rely=0.5)

    # def update(self):
    def playOffline(self):
        self.option["isPlaying"] = True
        self.setMenu()
        self.main.destroy()

    def setMenu(self):
        menubar = tk.Menu(self)
        menu1 = tk.Menu(menubar, tearoff=0)
        menu1.add_command(label="Open", command=alert)
        menu1.add_command(label="new", command=alert)
        menu1.add_separator()
        menu1.add_command(label="Quitter", command=self.destroy)
        menubar.add_cascade(label="Fichier", menu=menu1)
        menu2 = tk.Menu(menubar, tearoff=0)
        menu2.add_command(label="Copier", command=alert)
        menu2.add_command(label="Load", command=alert)
        menu2.add_separator()
        menu2.add_command(label="Export", command=alert)
        menubar.add_cascade(label="Editer", menu=menu2)
        menu6 = tk.Menu(menubar, tearoff=0)
        menu6.add_command(label="Theme", command=alert)
        menu6.add_command(label="Fullscreen", command=self.toggleFullScreenB)
        menubar.add_cascade(label="View", menu=menu6)
        menu7 = tk.Menu(menubar, tearoff=0)
        menu7.add_command(label="A propos", command=alert)
        menu7.add_command(label="Bestiaire", command=alert)
        menubar.add_cascade(label="Aide", menu=menu7)

        self.config(menu=menubar)

    def showGeneralOption(self):
        self.optionVar.set("General")
        tk.Label(self.opt, textvariable=self.optionVar).grid(row=0, column=0)
        tk.Label(self.opt, text="Test").grid(row=1, column=0)
        tk.Button(self.opt, text="AHah").grid(row=1, column=1)
        self.opt.grid(row=1, column=0)

    def showGraphiqueOption(self):
        self.optionVar.set("Graphique")
        tk.Label(self.opt, textvariable=self.optionVar).grid(row=0, column=0)
        self.opt.grid(row=1, column=0)

    def showThemeOption(self):
        self.optionVar.set("Theme")
        tk.Label(self.opt, textvariable=self.optionVar).grid(row=0, column=0)

        self.opt.grid(row=1, column=0)

    def showOption(self):
        self.main.destroy()

        self.info.destroy()
        self.main = tk.LabelFrame(
            self, text="Setting", bg=self.option["themeVar"]["backgroundFrame"]
        )
        self.optionVar = tk.StringVar()
        self.optionVar.set("General")
        self.opt = tk.LabelFrame(
            self.main, text="", bg=self.option["themeVar"]["backgroundFrame"]
        )
        self.main.grid()
        self.showGeneralOption()
        tk.Button(
            self.main,
            text="General",
            command=self.showGeneralOption,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=0, column=0, padx=5, pady=5)
        tk.Button(
            self.main,
            text="Graphique",
            command=self.showGraphiqueOption,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=0, column=2, padx=5, pady=5)
        tk.Button(
            self.main,
            text="theme",
            command=self.showThemeOption,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=0, column=4, padx=5, pady=5)

        tk.Button(
            self.main,
            text="Back",
            command=self.showStart,
            bg=self.option["themeVar"]["Button"],
            relief=tk.GROOVE,
            width=30,
        ).grid(row=2, column=2, padx=5, pady=5)
        self.main.place(in_=self, anchor="c", relx=0.5, rely=0.5)


if __name__ == "__main__":
    app = Application()
    app.title("Import Life 2.0")
    ##----- Programme principal -----##
    app.mainloop()  # Boucle d'attente des événements
