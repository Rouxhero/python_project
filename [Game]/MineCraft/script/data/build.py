tool = {
    "name": "Tools",
    "length": 2,
    "build": {
        "t001": {
            "name": "Wooden_Sword",
            "need": {
                "1003|1006": 2,
                "1001": 1,
            },
            "get": {
                "3001": 1,
            },
        },
        "t002": {
            "name": "Wooden_axe",
            "need": {
                "1003|1006": 3,
                "1001": 1,
            },
            "get": {
                "3002": 1,
            },
        },
    },
}
