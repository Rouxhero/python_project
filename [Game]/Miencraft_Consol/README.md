# Project By Rouxhero

Last commit : 23/09/2020

## How to run :
1. Download Project archive
2. Unzip and Open a Command line
    * for windows user : Windows + R , `cmd`
    * for linux user : Terminal
3. You needs Python3 to run game !
4. Do Cd command to be on the Main Repository
5. Type `Python main.py` or `Python3 main.py`
6. Enjoy :)