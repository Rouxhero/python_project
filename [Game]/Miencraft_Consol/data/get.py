
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from os import system

url = 'https://minecraft-ids.grahamedgecombe.com/'
req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
print('connection')
web_byte = urlopen(req).read()
print('uncoding')
webpage = web_byte.decode('utf-8')
print('reading')
soup = BeautifulSoup(webpage ,features="html.parser")
liste = []
for a in soup.find_all('span'):
	if a.get('class') == ["name"]:
		liste.append(a.text)
finalList = []
for item in liste:
	finalList.append("_".join(item.split(' ')))

print(finalList)