# coding: utf-8

from lxml import etree
from Item import *
from random import choice as C
from time import sleep as Pa


class Player:
    def __init__(self):
        self.Name = ""
        self.Life = 0
        self.Food = 0
        self.Armor = 0
        self.Emrald = 0
        self.inventaire = []
        self.inventaireListe = []
        self.CF = None
        self.CUF = None
        self.new = False

    def NewPlayer(self, Pseudo):
        self.Name = Pseudo
        self.new = True
        self.Life = 20
        self.Food = 14
        for i in ListeItem:
            self.inventaireListe.append(i)
            if i == "Log":
                self.inventaire.append([i, 5])
            else:
                self.inventaire.append([i, 0])

    def LoadPlayer(self, save):
        tree = etree.parse(save)
        print("Loading profil :", end="")
        for user in tree.xpath("/Save/player"):
            self.Name = user.get("Name")
            self.Life = int(user.get("Life"))
            self.Food = int(user.get("Food"))
            self.Armor = int(user.get("Armor"))
            self.Emrald = int(user.get("Emrald"))
        print("oOo", end="")
        for item in tree.xpath("/Save/player/inventaire/Item"):
            self.inventaire.append([item.get("name"), int(item.get("count"))])
            self.inventaireListe.append(item.get("name"),)
            print(C(["o", "O", "0", "@"]), end="")
        #CheckInvMaj
        for item in ListeItem :
            if item not in self.inventaireListe :
                self.inventaireListe.append(item)
                self.inventaire.append([item,0])
                print('\nVotre inventaire est mis a jours !')
    def Craft(self, i):
        i -= 1
        craftI = ListeCraft[self.CF][self.CUF][i]
        invOk = True
        print("Try to craft : " + craftI[2])
        for item in craftI[3]:
            place = ListeItem.index(item[0])
            Playercount = self.inventaire[place][1]
            if Playercount < item[1]:
                print("Il manque " + str(item[1] - Playercount) + " " + item[0] + " !!")
                invOk = False
        if invOk:
            print(
                "All good ,Crafting !\n->Time to craft : " + str(timeC[self.CF]) + "Sec"
            )
            for item in craftI[3]:
                place = ListeItem.index(item[0])
                self.inventaire[place][1] -= item[1]
            NewItmP = ListeItem.index(craftI[2])
            self.inventaire[NewItmP][1] += craftI[4]
            Pa(timeC[self.CF])
            print("Done !")

    def setInterface(self, ObjInterface):
        ObjInterface.inventaire = self.inventaire

    def SaveQ(self, nameS):
        self.nameS = nameS
        fileExE = True
        print("NewPlayer :" + str(self.new))
        if self.new:
            fileEx = True
            fileExE = True
            while fileEx:
                try:
                    print("Checking other save :")
                    open(self.nameS, "r")
                except FileNotFoundError:
                    print("Not save exist")
                    fileEx = False
                    fileExE = False
                print(fileEx)
                if fileEx:
                    self.new = False
                    choixx = input(
                        "Une partie Existe deja, voulais vous l'ecraser?(Y/n) : "
                    )
                    if choixx == "Y" or choixx == "y":
                        fileEx = False

                    else:
                        self.nameS = (
                            "../save/"
                            + input("Entrez votre nom de Sauvegrade :")
                            + "Wolrd.sv"
                        )
                        fileExE = False
        SaveDocs = (
            '<?xml version="1.0" encoding="UTF-8"?>\n\t<Save>\n\t\t<player Name="'
            + self.Name
            + '"\n\t\t\t\tLife="'
            + str(self.Life)
            + '"\n\t\t\t\tFood="'
            + str(self.Food)
            + '"\n\t\t\t\tArmor="'
            + str(self.Armor)
            + '"\n\t\t\t\tEmrald = "'
            + str(self.Emrald)
            + '">\n\t\t\t\t<inventaire>\n'
        )
        for Item in self.inventaire:
            SaveDocs += (
                '\t\t\t\t<Item name="' + Item[0] + '" count ="' + str(Item[1]) + '"/>\n'
            )
        open(self.nameS, "w").write(SaveDocs)
        if not fileExE:
            open("../save/SaveLibs.llG", "a").write(self.nameS[8:] + ";")
