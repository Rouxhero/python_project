ListeItem = [
    "Log",
    "Planck",
    "Stick",
    "Stone",
    "Diortite",
    "Andesite",
    "Granite",
    "Dirt",
    "Iron_Block",
    "Iron_Ingot",
    "Wooden_Axe",
    "Wooden_PicAxe",
    "Wooden_Hoe",
    "Wooden_Sword",
    "Wooden _howel",
    "Stone_Axe",
    "Stone_PicAxe",
    "Stone_Hoe",
    "Stone_Sword",
    "Stone_Showel",
    "Iron_Axe",
    "Iron_PicAxe",
    "Iron_Hoe",
    "Iron_Sword",
    "Iron_Showel",
    "Stone_Stair",	
	"Granite_Stair",
	"Andesite_Stair",
	"Diortite_Stair",
]
ListeCraft = [
    [
        [
            ["Primaire", "Wooden","Planck",[["Log", 1]], 4,],
            ["Primaire", "Wooden", "Stick", [["Planck", 2]], 4],
        ]
    ],
    [
        [
            ["Tools", "Wooden", "Wooden_Axe", [["Planck", 3], ["Stick", 2]], 1],
            ["Tools", "Wooden", "Wooden_PicAxe", [["Planck", 3], ["Stick", 2]], 1],
            ["Tools", "Wooden", "Wooden_Hoe", [["Planck", 2], ["Stick", 2]], 1],
            ["Tools", "Wooden", "Wooden_Sword", [["Planck", 2], ["Stick", 1]], 1],
            ["Tools", "Wooden", "Wooden_Showel", [["Planck", 1], ["Stick", 2]], 1],
        ],
        [
            ["Tools", "Stone", "Stone_Axe", [["Stone", 3], ["Stick", 2]], 1],
            ["Tools", "Stone", "Stone_PicAxe", [["Stone", 3], ["Stick", 2]], 1],
            ["Tools", "Stone", "Stone_Hoe", [["Stone", 2], ["Stick", 2]], 1],
            ["Tools", "Stone", "Stone_Sword", [["Stone", 2], ["Stick", 1]], 1],
            ["Tools", "Stone", "Stone_Showel", [["Stone", 1], ["Stick", 2]], 1],
        ],
        [
            ["Tools", "Iron", "Iron_Axe", [["Iron_Ingot", 3], ["Stick", 2]], 1],
            ["Tools", "Iron", "Iron_PicAxe", [["Iron_Ingot", 3], ["Stick", 2]], 1],
            ["Tools", "Iron", "Iron_Hoe", [["Iron_Ingot", 2], ["Stick", 2]], 1],
            ["Tools", "Iron", "Iron_Sword", [["Iron_Ingot", 2], ["Stick", 1]], 1],
            ["Tools", "Iron", "Iron_Showel", [["Iron_Ingot", 1], ["Stick", 2]], 1],
        ],
    ],
    [
        [
            ["Block", "Stair", "Stone_Stair", [["Stone", 6]], 4],
            ["Block", "Stair", "Granite_Stair", [["Granite", 6]], 4],
            ["Block", "Stair", "Andesite_Stair", [["Andesite", 6]], 4],
            ["Block", "Stair", "Diortite_Stair", [["Diortite", 6]], 4],
        ]
    ],
]
listeFami = ["Primaire", "Tools", "Block", "Fourniturs"]
timeC = [1, 5, 3, 2]
listeUnderM = [["Wooden"], ["Wooden", "Stone", "Iron"], ["Stair"], []]
ListeCF = ["  / / /  ", "### /  / ", "#  ## ###", "   ## ## "]
