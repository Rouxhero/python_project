from module import *
from gameO import *
from Interface import *
from Player import *


def Load():
    end = False
    choix = -1
    window.clear()
    window.ShowSave()
    while not end:
        try:
            choix = int(input("Entrez le Numero :"))
        except ValueError:
            window.clear()
            window.ShowSave()
            print("Invalide menu !!")
        if choix > 1 and choix < len(window.listeofsave) + 1:
            choix -= 2
            game = MainGame()
            player = Player()
            saveP = "../save/" + window.listeofsave[choix]
            player.LoadPlayer(saveP)
            game.LoadGame(saveP)
            game.setInterface(window)
            player.setInterface(window)
            window.world = game
            window.player = player
            end = True
            ret = 1
        elif choix == 1:
            end = True
            ret = 0
        else:
            print("Invalide Save !")
    return ret


def Setting():
    print("Comming Soon")


def newWorld():
    game = MainGame()
    window.clear()
    game.NewGameConfig()
    player = Player()
    player.NewPlayer(game.PlayerName)
    game.GenFor()
    game.GenVil()
    game.GenMin()
    game.setInterface(window)
    player.setInterface(window)
    window.world = game
    window.player = player


def Lobby():
    window.clear()
    window.Lobby()
    life = (
        "| Life : " + "#" * window.player.Life + "O" * (20 - window.player.Life) + "|"
    )
    food = (
        "\n| Food : " + "#" * window.player.Food + "O" * (20 - window.player.Food) + "|"
    )
    Armor = (
        "\n| Armor: "
        + "#" * window.player.Armor
        + "O" * (20 - window.player.Armor)
        + "|"
    )
    maxL = max([len(life), len(food), len(Armor)])
    print("+" + "-" * (maxL - 3) + "+")
    print(life, food, Armor)
    print("+" + "-" * (maxL - 3) + "+")


def heal():
    print("Heal soon")


def travel():
    print(window.world.location)


def ShowCraft():
    window.clear()
    window.ShowFamily()


def ShowCraftI(i):
    window.clear()
    window.ShowCraft(i)


def ShowUCraftI(i):
    window.clear()
    window.ShowUnderCraft(i)


def CraftI(i):
    window.player.Craft(i)


def invv():
    window.clear()
    window.ShowInv()


def saveQ():
    print("Saving !")
    window.player.SaveQ(window.world.save)
    window.world.saveQ(window.player.nameS)
    print("Done !")
    print("adios amigos!")
    exit()


def GameP():
    end = False
    choix = 0
    ListeOfFunc = [
        [Lobby, invv, travel, heal, saveQ],
        [invv, ShowCraft, Lobby],
        [ShowCraft],
    ]
    while not end:
        try:
            if window.PlayerPlace == 2:
                if choix == 1:
                    invv()
                else:
                    ShowCraftI(choix - 1)
            elif window.PlayerPlace == 3:
                if choix == 1:
                    ShowCraft()
                else:
                    if choix - 1 > 0 and choix - 1 <= len(
                        window.UnderFamily[window.choixC]
                    ):
                        ShowUCraftI(choix - 1)
                    else:
                        print("Invalide SubMenu")
            elif window.PlayerPlace == 4:
                if choix == 1:
                    ShowCraftI(0)
                else:
                    if choix - 1 > 0 and choix < (
                        len(ListeCraft[window.choixC][window.player.CUF]) + 2
                    ):
                        CraftI(choix - 1)
                    else:
                        print(
                            "Invalide Craft, need to be in [2-"
                            + str(
                                (len(ListeCraft[window.choixC][window.player.CUF])) + 1
                            )
                            + "]"
                        )
            else:
                try:
                    ListeOfFunc[window.PlayerPlace][choix]()
                except IndexError:
                    ListeOfFunc[window.PlayerPlace][0]()
                    print("Invalide Menu !!")
        except AttributeError as e:
            ListeOfFunc[window.PlayerPlace][0]()
            print(window.PlayerPlace, choix, e)
            print("Retry Entry")
        try:
            choix = int(input("Entrez votre choix : "))
        except ValueError:
            print("Invalide Menu !")
            choix = 0


window = GameInterface()
window.clear()
window.Menu()
choix = 0
while choix != 4:
    choix = int(input("Entrez votre choix : "))
    if choix == 1:
        newWorld()
        GameP()
    elif choix == 2:
        res = Load()
        if res == 1:
            GameP()
        else:
            window.clear()
            window.Menu()
    elif choix == 3:
        Setting()
    elif choix == 4:
        pass
    else:
        print("Invalide Menu !")
print("adios amigos!")
exit()
