# Made by rouxhero
from os import system as shell
import os


class GameInterface:
    def __init__(self):
        self.os = os.name
        if self.os != "posix":
            shell("color a")
        self.Structure = []
        self.inventaire = []
        self.ItemListe = []
        self.CraftList = []
        self.FamilyList = []
        self.UnderFamily = []
        self.CF = []
        self.Fam = ""
        self.choixC = 2
        self.PlayerPlace = 0
        self.world = None
        self.player = None

    def clear(self):
        if self.os == "posix":
            shell("clear")
        else:
            shell("cls")

    def ShowUnderCraft(self, choix):
        choix -= 1
        self.player.CF = self.choixC
        self.player.CUF = choix
        self.PlayerPlace = 4
        if len(self.Fam) // 2 == len(self.Fam) / 2:
            x = 21
        else:
            x = 20
        print(
            """
             ###########################################  + --+---+---+
             #             Liste des Craft :           #  | """
            + self.CF[self.choixC][0]
            + """ | """
            + self.CF[self.choixC][1]
            + """ | """
            + self.CF[self.choixC][2]
            + """ |
             #"""
            + " " * (20 - (len(self.Fam)) // 2)
            + self.Fam
            + " " * (x - (len(self.Fam)) // 2)
            + """#  +---+---+---+
             #                                         #  | """
            + self.CF[self.choixC][3]
            + """ | """
            + self.CF[self.choixC][4]
            + """ | """
            + self.CF[self.choixC][5]
            + """ |
             #    Entrer le numero du craft souhaité   #  +---+---+---+
             #                    1-Retour             #  | """
            + self.CF[self.choixC][6]
            + """ | """
            + self.CF[self.choixC][7]
            + """ | """
            + self.CF[self.choixC][8]
            + """ |
             ###########################################  +---+---+---+
             """
        )
        top_Bot = "+"
        body = "| "
        for item in self.FamilyList:
            if item == self.Fam:
                top_Bot += "-" * (len(item) + 4) + "+"
                body += "->" + item + " | "
            else:
                top_Bot += "-" * (len(item) + 2) + "+"
                body += item + " | "
        print("         " + top_Bot)
        print("         " + body)
        print("         " + top_Bot)
        underListe = self.UnderFamily[self.choixC]
        UnderFam = self.UnderFamily[self.choixC][choix]
        top_Bot = "+"
        body = "| "
        for item in underListe:
            if item == UnderFam:
                top_Bot += "-" * (len(item) + 4) + "+"
                body += "->" + item + " | "
            else:
                top_Bot += "-" * (len(item) + 2) + "+"
                body += item + " | "
        print("         " + top_Bot)
        print("         " + body)
        print("         " + top_Bot)
        Liste = []
        count = 2
        for item in self.CraftList[self.choixC][choix]:  # Recuperation des Items
            Liste.append(item)
        count = 2
        for item in Liste:
            lenT = len(item[2]) + 3 + len(str(count))
            maxLen = lenT
            Needs = []
            for need in item[3]:
                line = "| " + need[0] + " x " + str(need[1])
                maxLen = max(maxLen, (len(line)) + 2)
                Needs.append(line)
            Title = "+" + "-" * maxLen + "+\n"
            Title += "| " + str(count) + "-" + item[2] + " " * (maxLen - lenT) + " | \n"
            Title += "+" + "-" * maxLen + "+"
            print(Title)
            for Item in Needs:
                print(Item + " " * (maxLen - len(Item)) + " |")
                print("+" + "-" * maxLen + "+")
            count += 1

    def ShowCraft(self, choix):
        if choix != 0:
            choix -= 1
            self.choixC = choix
        else:
            choix = self.choixC
        try:
            self.Fam = self.FamilyList[choix]
        except IndexError as e:
            print("Erreur Mauvaise Selection : ", e)
            return 0
        self.PlayerPlace = 3
        if len(self.Fam) // 2 == len(self.Fam) / 2:
            x = 21
        else:
            x = 20
        print(
            """
             ###########################################  + --+---+---+
             #             Liste des Craft :           #  | """
            + self.CF[choix][0]
            + """ | """
            + self.CF[choix][1]
            + """ | """
            + self.CF[choix][2]
            + """ |
             #"""
            + " " * (20 - (len(self.Fam)) // 2)
            + self.Fam
            + " " * (x - (len(self.Fam)) // 2)
            + """#  +---+---+---+
             #                                         #  | """
            + self.CF[choix][3]
            + """ | """
            + self.CF[choix][4]
            + """ | """
            + self.CF[choix][5]
            + """ |
             #    Entrer le numero du craft souhaité   #  +---+---+---+
             #                    1-Retour             #  | """
            + self.CF[choix][6]
            + """ | """
            + self.CF[choix][7]
            + """ | """
            + self.CF[choix][8]
            + """ |
             ###########################################  +---+---+---+
             """
        )
        count = 2
        top_Bot = "+"
        body = "| "
        for item in self.FamilyList:
            if item == self.Fam:
                top_Bot += "-" * (len(item) + 4) + "+"
                body += "->" + item + " | "
            else:
                top_Bot += "-" * (len(item) + 2) + "+"
                body += item + " | "
            count += 1
        print("         " + top_Bot)
        print("         " + body)
        print("         " + top_Bot)
        underListe = self.UnderFamily[choix]
        count = 2
        top_Bot = "+"
        body = "| "
        for item in underListe:
            top_Bot += "-" * (len(item) + 4) + "+"
            body += str(count) + " " + item + " | "
            count += 1
        print("         " + top_Bot)
        print("         " + body)
        print("         " + top_Bot)

    def ShowFamily(self):
        print(
            """
             ###########################################  + --+---+---+
             #             Liste des Craft :           #  |   | 0 | 0 |
             #                                         #  +---+---+---+
             #    Entrer le numero du menu souhaité    #  |   | / | 0 |
             #                                         #  +---+---+---+
             #                    1-Retour             #  |   | / |   |
             ###########################################  +---+---+---+
             """
        )
        self.PlayerPlace = 2
        count = 2
        top_Bot = "+"
        body = "| "
        for item in self.FamilyList:
            top_Bot += "-" * (len(item) + 4) + "+"
            body += str(count) + " " + item + " | "
            count += 1
        print("         " + top_Bot)
        print("         " + body)
        print("         " + top_Bot)

    def ShowInv(self):
        self.PlayerPlace = 1
        print(
            """
             ###########################################  + --+---+---+
             #                Inventaire               #  |   |   |   |
             #                                         #  +---+---+---+
             #    1-Afficher les crafts                #  |   |   |   |
             #                                         #  +---+---+---+
             #                    2-Lobby              #  |   |   |   |
             ###########################################  +---+---+---+
             """
        )
        maxlenI = 0
        maxlenN = 0
        for i in self.inventaire:
            maxlenI = max(len(i[0]), maxlenI)
            maxlenN = max(len(str(i[1])), maxlenN)
        maxlenI += 2
        inven = "+" + "-" * (maxlenI + 2) + "+" + "-" * (maxlenN+2) + "+\n"
        for i in self.inventaire:
            if i[1] > 0:
                inven += (
                    "| "
                    + i[0]
                    + " " * (maxlenI - len(i[0]))
                    + " |  "
                    + " " * (maxlenN - len(str(i[1])))
                    + str(i[1])
                    + "| \n"
                )
                inven += "+" + "-" * (maxlenI + 2) + "+" + "-" * (maxlenN+2) + "+\n"
        print(inven)

    def Menu(self):
        print(
            """
            #############################
            #    MineCraft Console      #
            #                           #
            #          1- New Game      #
            #                           #
            #     2- Load               #
            #                           #
            #          3- Setting       #
            #                           #
            #     4- Quit               #
            #                           #
            #############################
            Made by Rouxhero
        """
        )

    def Lobby(self):
        self.PlayerPlace = 0
        print(
            """
            ########################################### 
            #                  Maison                 #
            #                                         #  
            #    1-Aller dans l'inventaire/craft      #
            #        2-Voyager quelque part           #  
            #             3-Soin/cuisine              # 
            #               4-Save and quit           # 
            #                                         # 
            ########################################### 
        +--------------+
        |    Stats     |
        +--------------+
        """
        )

    def ShowSave(self):
        print(
            """
        +-------------------------------------------+
        |   Entrez le Numero  de la sauvegarde :    |
        |   1-Menu Principale                       |
        +-------------------------------------------+
            """
        )
        try:
            saveL = open("../save/SaveLibs.llG", "r").read()
        except FileNotFoundError:
            print("Fichier Manquant , retelecharger le jeu svp !")
        self.listeofsave = saveL.split(";")
        maxL = 0
        for save in self.listeofsave:
            if len(save) != 0:
                savee = save[: len(save) - 8]
                maxL = max(maxL, len(savee))
        line = "+" + "-" * (maxL + 5) + "+"
        print(line)
        for save in range(len(self.listeofsave) - 1):
            savee = self.listeofsave[save][: len(self.listeofsave[save]) - 8]
            print(
                "| "
                + " " * (2 - len(str(save + 2)))
                + str(save + 2)
                + "-"
                + savee
                + " " * (maxL - len(savee))
                + " |"
            )
            print(line)
