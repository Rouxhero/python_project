# Made by rouxhero
# coding: utf-8

from lxml import etree
from Item import *
from random import randint as r
from random import choice as C
import re


class MainGame:
    def __init__(self):
        self.location = []
        self.save = ""

    def GenFor(self):
        name = "Forest"
        loca = r(150, 450)
        nbOak = r(10, 33)
        nbBrich = r(10, 33)
        nbDarkOak = r(10, 33)
        self.location.append([name, loca, [nbOak, nbBrich, nbDarkOak]])
        print("######", end="")

    def GenVil(self):
        name = "Vilage"
        loca = r(150, 450)
        forge = C([True, False])
        abatoire = C([True, False])
        berger = C([True, False])
        armurier = C([True, False])
        mineur = C([True, False])
        marchant = C([True, False])
        self.location.append(
            [name, loca, [forge, abatoire, berger, armurier, mineur, marchant]]
        )
        print("######", end="")

    def GenMin(self):
        name = "Mine"
        loca = r(150, 450)
        data = []
        self.location.append([name, loca, data])
        print("######")

    def setInterface(self, ObjInterface):
        ObjInterface.Structure = self.location
        ObjInterface.ItemList = ListeItem
        ObjInterface.CraftList = ListeCraft
        ObjInterface.FamilyList = listeFami
        ObjInterface.CF = ListeCF
        ObjInterface.UnderFamily = listeUnderM

    def NewGameConfig(self):
        print("Configuration de la nouvelle partie :")
        NameOk = False
        valideName = r"[A-Za-z0-9][^ ]+"
        while not NameOk:
            self.PlayerName = input("Entrez votre pseudo : ")
            if self.PlayerName == "":
                print("Nom Vide !")
            elif len(self.PlayerName) < 5 or len(self.PlayerName) > 18:
                print("Longueur non Valide ! min 5Lettre , Max 18 Lettre")
            else:
                NameOk = True
        self.save = input(
            "Entrez votre nom de Sauvegrade(Pars default :'"
            + self.PlayerName
            + "World.sv' : "
        )
        if self.save == "":
            self.save = "../save/" + self.PlayerName + "Wolrd.sv"

    def LoadGame(self, save):
        tree = etree.parse(save)
        print('\nLoading world :',end='')
        self.save = save
        for Place in tree.xpath("/Save/world/place"):
            print(C(["w", "W", "uu", "Vv"]), end="")
            name = Place.get("Name")
            loca = int(Place.get("locat"))
            dataS = []
            for data in Place.getchildren():
                print(C(["w", "W", "uu", "Vv"]), end="")
                if name == "Forest":
                    dataS.append(int(data.get("info")))
                elif name == "Vilage":
                    dataS.append(bool(data.get("info")))
                else:
                    dataS.append(data.get("info"))
            self.location.append([name, loca, dataS])
        input("\nDone ! Appuyer surs Entrer ")

    def saveQ(self, nameS):
        if nameS != self.save:
            self.save = nameS
        SaveDocs = """
        </inventaire>
    </player>

    <world>\n"""
        for Place in self.location:
            SaveDocs += (
                '\t\t<place Name="' + Place[0] + '" locat="' + str(Place[1]) + '">\n'
            )
            for Info in Place[2]:
                SaveDocs += '\t\t\t<data info="' + str(Info) + '"/>\n'

            SaveDocs += "\t\t</place>\n"
        SaveDocs += """
       </world>
</Save>
        """
        open(self.save, "a").write(SaveDocs)
