#  Copyright (c) 2021.  Made By Rouxhero
import pygame
from pygame.locals import *
from util.module import *
from util.keyBind import *
MENU_FPS = 5


class menu:

    def __init__(self, width, height):
        pygame.init()
        self.screen = {"Width": width, "height": height}
        self.surface = pygame.display.set_mode((width, height))
        pygame.font.init()
        self.font = pygame.font.SysFont("comicsansms", 50)
        self.font2 = pygame.font.SysFont("comicsansms", 25)
        pygame.key.set_repeat(400, 30)
        self.stop = False
        pygame.time.Clock().tick(MENU_FPS)
        pygame.display.update()

    def show(self):
        self.surface.fill(BLACK)
        head = self.font.render(
            "SquareGame",
            True,
            (color(),color(),color()),
        )
        self.surface.blit(head, (195, 100))
        pygame.display.update()
        pygame.time.Clock().tick(MENU_FPS)


menuee = menu(700, 600)
while not menuee.stop:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            menuee.stop = True
        if event.type == MOUSEMOTION :
            pass
            # print(event.pos[0],",",event.pos[1])
        if event.type == KEYDOWN:
            print(keyBinding[event.key])

    menuee.show()

exit()
