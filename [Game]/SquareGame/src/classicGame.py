#  Copyright (c) 2021.  Made By Rouxhero

import pygame
from pygame.locals import *
from util.player import Player
from util.module import *
from random import randint as r


class ClassicGame:

    def __init__(self, width, height):
        pygame.init()
        self.screen = {"Width": width, "height": height}
        self.surface = pygame.display.set_mode((width, height))
        pygame.font.init()
        self.font = pygame.font.SysFont("comicsansms", 50)
        self.font2 = pygame.font.SysFont("comicsansms", 25)
        pygame.key.set_repeat(400, 30)
        self.player = Player()
        self.continu = True
        self.play = 1

    def mainGame(self):
        # Player
        self.player.updatePlayer()
        self.player.spawn()

        # Text
        self.surface.fill(BLACK)
        xp = self.font2.render(
            "xp : " + str(self.player.xp // 1) + "/" + str(self.player.level ** 2),
            True,
            GREY,
        )
        score = self.font2.render(
            "score : " + str(self.player.score // 1),
            True,
            GREY,
        )
        levl = self.font2.render(
            "Level : " + str(self.player.level),
            True,
            WHITE,
        )
        self.addBonus()
        self.surface.blit(self.player.img, (self.player.x, self.player.y))
        self.surface.blit(xp, (0, 0))
        self.surface.blit(levl, (250, 0))
        self.surface.blit(score, (500, 0))
        pygame.time.Clock().tick(self.player.level * 2 + 60)
        pygame.display.update()

    def addBonus(self):
        for i in self.player.bonus:
            self.surface.blit(i[0], i[2])

    def pause(self):
        pauseTxt = self.font.render("pauseTxt", True, (color(), color(), color()))
        score = self.font2.render("Level : " + str(self.player.level), True, WHITE)
        self.surface.fill(BLACK)
        self.surface.blit(pauseTxt, (250, 250))
        self.surface.blit(score, (275, 320))
        pygame.time.Clock().tick(2)
        pygame.display.update()

    def GameOver(self):
        loose = self.font.render("Game Over !", True, (color(), color(), color()))
        level = self.font2.render(
            "Press 'Space' to quit or 'r' to restart", True, WHITE
        )
        score = self.font2.render(
            "score : " + str(self.player.score // 1),
            True,
            (155, 255, 255),
        )
        self.surface.fill((0, 0, 0))
        self.surface.blit(loose, (200, 200))
        self.surface.blit(level, (150, 320))
        self.surface.blit(score, (250, 270))

        pygame.time.Clock().tick(2)
        pygame.display.update()
