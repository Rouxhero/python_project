#  Copyright (c) 2021.  Made By Rouxhero

import pygame
from util.module import color
from random import randint as r


class Player:
    def __init__(self):
        self.x = 30
        self.y = 30
        self.img = pygame.Surface((30, 30))
        self.img.fill((100, 100, 100))
        self.Xi = 1
        self.Yi = 0
        self.xp = 0
        self.score = 0
        self.level = 1
        self.gameOver = False
        self.bonus = []
        self.coefSpeed = 1

    def run(self):
        if self.coefSpeed != 1:
            self.coefSpeed = 1
        else :
            self.coefSpeed = 5

    def left(self):
        self.Xi = -1
        self.Yi = 0

    def right(self):
        self.Xi = 1
        self.Yi = 0

    def up(self):
        self.Yi = -1
        self.Xi = 0

    def down(self):
        self.Yi = 1
        self.Xi = 0

    def updatePlayer(self):
        if self.xp >= self.level ** 2:
            self.xp = self.xp - self.level ** 2
            self.level += 1
            self.score += 5
        self.x += self.coefSpeed * self.Xi
        self.y += self.coefSpeed * self.Yi
        self.img = pygame.Surface((self.level + 30, self.level + 30))
        self.img.fill((155, 105, 155))

    def check(self):
        if self.x < 0 or self.y < 0 or self.x > 700 or self.y > 600:
            self.gameOver = True
        for i in self.bonus:
            if (
                    i[2][0] >= self.x
                    and i[2][1] >= self.y
                    and i[3][0] <= self.x + self.img.get_rect()[2]
                    and i[3][1] <= self.y + self.img.get_rect()[3]
            ):
                self.xp += i[1]
                self.score += 1
                self.bonus.pop(self.bonus.index(i))

    def spawn(self):
        doIspawn = r(0, 300)
        if doIspawn <= 2:
            bonus = r(1, r(5, 15))
            x, y = r(0, 700), r(0, 600)
            colors = (color(), color(), color())
            img = pygame.Surface((bonus % 40, bonus % 40))
            img.fill(colors)
            self.bonus.append([img, bonus, (x, y), ((x + bonus % 40), (y + bonus % 40))])
