#  Copyright (c) 2021.  Made By Rouxhero

from random import randint as r

color = lambda: r(0, 255)
BLACK = (0, 0, 0)
GREY = (155, 155, 155)
WHITE = (255, 255, 255)
