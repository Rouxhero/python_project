# -*- coding: utf-8 -*-

# Class to create SaveApp

def uncode(line):
    booleanDic = {
        "True": True,
        "true": True,
        "False": False,
        "false": False,
    }
    if line in booleanDic:
        return booleanDic[line]
    else:
        try:
            return int(line)
        except ValueError:
            return line


class Files:
    def __init__(self, name, bools):
        if bools:
            self.name = "../../data/" + name
        else:
            self.name = "../"+name
        self.file = """#Files Generator\n"""
        self.option = {}

    @staticmethod
    def load(file, bools):
        if bools:
            file = "../data/" + file
        else :
            file = "../" + file
        open(file, 'a')
        with open(file, 'r')as files:
            files = files.read()
            return Files.getAllOptionFile(files)

    def init(self):
        try:
            self.option = self.loadAllOption()
        except FileNotFoundError:
            with open(self.name, "w") as f:
                f.write(self.file)
                self.option = {}
            return "[File][Error] : Files doesn't exist! => File created"
        return "[File][Succes] Files " + self.name + " open !"

    def addOption(self, name, value):
        self.option[name] = value
        return "[File][Succes] Option added !"

    def reloadOption(self, dico):
        self.option = dico

    def loadAllOption(self):
        option = {}
        key = None
        lines = open(self.name, "r").read().split("\n")
        for line in lines:
            if len(line) > 1:
                if line[0] != "#":
                    if line[0] == "-":
                        key = line[1:]
                    else:
                        if key is not None:
                            line = uncode(line)
                            option[key] = line
                            key = None
        return option

    def getAllOption(self):
        return self.option

    def saveFiles(self):
        for key in self.option:
            self.file += "-" + key + "\n" + str(self.option[key]) + "\n"
        with open(self.name, "w") as f:
            f.write(self.file)
        self.file = """#Files Generator\n"""
        return "[File][Succes] Config files saved at : " + self.name

    @staticmethod
    def getAllOptionFile(files):
        option = {}
        key = None
        lines = files.split("\n")
        for line in lines:
            if len(line) > 1:
                if line[0] != "#":
                    if line[0] == "-":
                        key = line[1:]
                    else:
                        if key is not None:
                            line = uncode(line)
                            option[key] = line
                            key = None
        return option


