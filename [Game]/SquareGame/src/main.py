#  Copyright (c) 2021.  Made By Rouxhero

from classicGame import ClassicGame
import pygame
from util.file import *
import os

from pygame.locals import *

os.system('cd ~%dpt0')
config = Files.load("config.cfg",True)
game = ClassicGame(config["Width"], config["Height"])
joueur = game.player
while game.continu:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game.continu = False
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LSHIFT:
                joueur.run()
            if event.key == pygame.K_s:
                joueur.down()
            if event.key == pygame.K_w:
                joueur.up()
            if event.key == pygame.K_d:
                joueur.right()
            if event.key == pygame.K_a:
                joueur.left()
            if event.key == pygame.K_SPACE:
                joueur.xp += 200
                if joueur.gameOver:
                    exit()
            if event.key == pygame.K_RETURN:
                game.play = game.play * -1
            if event.key == pygame.K_r:
                if joueur.gameOver:
                    game = ClassicGame(700,600)
                    joueur = game.player

    if game.play == 1 and not joueur.gameOver:
        game.mainGame()
        joueur.check()

    elif game.play == -1 and not joueur.gameOver:
        game.pause()

    elif joueur.gameOver:
        game.GameOver()


exit()
