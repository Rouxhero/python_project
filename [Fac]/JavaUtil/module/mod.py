import os

class Core :


	def __init__(self):
		self.data = {
			"dir":"E:/fac/L2/S4/projet",
			"files":[],
			"dirs":{},
		}

		self.loadDir()


	def setDir(self,path):
		self.data['dir'] = path
		self.loadDir()

	def loadDir(self):
		self.data["files"] = []
		self.data['dirs'] = {}
		for path, dirs, files in os.walk(self.data['dir']):
			self.data['dirs'][path] = [dirs,files]
			for file in files:
				self.data['files'].append(path+"\\"+file)
			print(path,dirs,files)
		print(self.data)
