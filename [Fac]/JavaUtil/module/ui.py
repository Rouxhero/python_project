import tkinter as tk
from tkinter import ttk
from mod import Core
from tkinter.filedialog import *

class windows(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.core = Core()
        self.wm_title("Java Utilitaire")
        self.container = tk.Frame(self, height=400, width=600)
        self.container.grid_rowconfigure(0, weight=1)
        self.container.grid_columnconfigure(0, weight=1)
        self.showWorkingDir()

    def showWorkingDir(self):
	    scrollbar = tk.Scrollbar(self.container)
	    scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
	    data = tk.Listbox(self.container, yscrollcommand=scrollbar.set, width=60)
	    allFile = self.core.data['dirs']
	    for dirs in allFile:
	    	nbID = dirs.split("\\")
	    	data.insert(tk.END," "*(len(nbID))+nbID[-1])
	    	for file in allFile[dirs][1]:
	    		data.insert(tk.END,"   "*(len(nbID))+"|" )
	    		data.insert(tk.END,"   "*(len(nbID))+"L "+file )
	    	data.insert(tk.END,"   "*(len(nbID))+"|" )
	    	
	        # nbOfID = allFile[file].split('\\')
	        
	    data.pack(side=tk.LEFT, fill=tk.BOTH)
	    scrollbar.config(command=data.yview)
	    self.container.pack(side="top", fill="both", expand=True)


    def __selectNewFolder(self):
    	paths = askdirectory()
    	self.core.setDir(paths)


if __name__ == "__main__":
    testObj = windows()
    testObj.mainloop()