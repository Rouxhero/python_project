from tkinter.filedialog import *
from module.module import *
import module.uml as uml
import os

# Un Repertoir ou un seul fichiers
oneFile = False

# Affiche les package
showPackage = True

# Affiche les relation entre class
showRela = True


def doOneFile(path,theUml):
	with open(path,'r') as fichir :
		file = fichir.read()
		uml = convert(file)
		data = ""
		try :
			data = uml.run()
		except JavaFilesExecption as e:
			print("Error : ",e)
		theUml.addData(data)
		

if __name__ == '__main__':
	theUml = uml.Uml(showPackage,showRela)
	if oneFile :
		path = askopenfilename(title="Ouvrir le main java",filetypes=[('java files','.java'),('all files','.*')])
		doOneFile(path,theUml)
		theUml.save()
	else :
		paths = askdirectory()
		for path, dirs, files in os.walk(paths):
			for file in files:
				end = file.split('.')[1]
				if end == "java":
					doOneFile(path+"/"+file,theUml)
		theUml.save()















