class Uml :

	def __init__(self,showPackage=False,showRela=True):
		self.package = []
		self.showPackage = showPackage
		self.showRela = showRela
		self.data = {}

	def __addPackage(self,package):
		self.package.append(package)
		self.data[package] = []

	def addData(self,data):
		assert(type(data) == dict)
		if not data['package'] in self.package:
			self.__addPackage(data['package'])
		self.data[data['package']].append(data)


	def __generateTxt(self):
		if not self.showRela :
			txt  = "@startuml\nleft to right direction\n"
		else :
			txt  = "@startuml\n"
		count = 0
		prefix = lambda :"\t"*count
		link = {}
		extends = []
		for package in self.package :
			if self.showPackage:
				txt += "package "+package + "{\n"
				count += 1
			for data in self.data[package]:
				if data['head'] != "":
					txt+= prefix()+data['head']+"{\n"
					count += 1
					for line in data['attributes'] :
						txt += prefix() + line + "\n"
					for line in data['methode'] :
						txt += prefix() + line + "\n"
					if data["father"] != []:
						extends.append(data["father"])
					link[data["name"]]= data['link']
					count -= 1
					txt += prefix() +'}\n'
			if self.showPackage:
				count -= 1
				txt += prefix() +'}\n'
		if self.showRela :
			for key in link:
				for ref in link[key]:
					txt += prefix() + key + " -- " + ref +"\n"
		for key in extends:
				txt += prefix() + key[0] + key[2] + key[1] +"\n"
		
		txt += '@enduml'
		return txt


				


	def save(self,name="temp"):
		with open(name+".wsd","w") as fichier:
			fichier.write(self.__generateTxt())