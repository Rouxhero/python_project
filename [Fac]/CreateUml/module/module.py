from module.data import *
from module.erreur import *
import re

class convert:

	def __init__(self,filesText):
		self.txt = filesText
		self.data = {"package":[],"name":"","head":"","attributes":[],"methode":[],'link':[],"father":[]}
		


	def run(self):
		self.__getPackage()
		self.__getClassName()
		self.__getattribute()
		self.__getMethode()
		return self.data


	def __getPackage(self):
		data = re.findall(package,self.txt)
		if len(data)>0:
			self.data['package']  = data[0].split(' ')[1]
		else :
			raise JavaFilesExecption("No package found !")

	def __getMethode(self):
		data = re.findall(methode, self.txt)
		methodeData = []
		for line in data :
			index = self.__getStartIndex(line)
			line = line[index:-1]
			if not line[1] in listJava and not line[1] in self.data['link'] :
				self.data['link'].append(line[1])
			methodeData.append(dicoSecu[line[0]] + " " + " ".join(line[2:])+ " : " + line[1])
		self.data['methode'] = methodeData
		
	def __getattribute(self):
		data = re.findall(attributes, self.txt)
		attributeData = []
		for line in data :
			i = 2
			index = self.__getStartIndex(line)
			line = line[index:-1]
			if "final" in line :
				i+=1
			if "static" in line :
				line[line.index("static")] = dicoSpe["static"]
			types = self.__checkList(line)
			for typess in types:
				if not typess in listJava and not typess in self.data['link'] :
					self.data['link'].append(typess)
			attributeData.append(dicoSecu[line[0]] + " " + " ".join(line[i:]))
		self.data['attributes'] = attributeData

	def __getClassName(self):
		data = re.findall(className,self.txt)
		if len(data)>0:
			data = data[0]
			
			self.data['head'] = " ".join(data[2:4])
			if "class" in data:
				self.data['name']= data[data.index('class')+1]
			elif  "interface" in data :
				self.data['name']= data[data.index('interface')+1]
			else :
				self.data['name']= data[data.index('enum')+1]
			if data[-1] == "implements":
				father = [data[3],data[-2].split(' ')[1],"-->"]
				
				self.data['father'] = father
			elif data[-1] == "extends":
				name = data[-2].split(' ')[1]
				if not name in javaClasique :
					father = [data[3],name,"--|>"]
					self.data['father'] = father

	def __getStartIndex(self,line):
		for key in dicoSecu:
			if key in line:
				return line.index(key)
		return -1

	def __checkList(self,line):
		types = []
		word = ""
		for lines in line:
			if "<" in lines :
				i = lines.index('<')+1
				word = ""
				while lines[i] != ">" and len(lines)>i:
					if lines[i] != ",":
						word += lines[i]
					else :
						types.append(word)
						word = ""
					i += 1
		types.append(word)
		return types




