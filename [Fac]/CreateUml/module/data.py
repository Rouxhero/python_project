dicoSecu = {"protected":"#", "public":"+","private":"-"}
dicoSpe = {"abstract":"{abstract}","static":"{static}"}
listJava  = ['int','String','void','boolean','Integer','float','']
className = r'(public)\s(abstract)?\s?(class|interface|enum)\s([A-Z][a-zA-Z]*)\s?\s?((extends|implements)\s[a-zA-Z]+)?'
#            abstract ?               security                  type ?               name                 argu                                        
methode = r"(abstract\s)?(protected|public|private)\s?([a-zA-Z\[\]<,>]+)?\s([a-zA-Z][a-zA-Z]*)\s?(\(([a-zA-Z]+\s[a-zA-Z]+,?\s?)*\))"
"extends Vehicle"
#                  security                   final ?   static?        type            name
attributes = r"(protected|public|private)\s(final\s)?(static\s)?([a-zA-Z\[\]<,>]+)\s([a-zA-Z][a-zA-Z1-9_]*)(;|\s=)"
package = r"package\s[a-zA-Z.]*"

javaClasique = ['Exception']
