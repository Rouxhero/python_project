# Python Project
<img src="https://th.bing.com/th/id/OIP.TazMmXhzKAekUm-TXh3w3AHaHa?w=179&h=180&c=7&o=5&pid=1.7" alt="logo">

## Author :
-[RouxHero](https://gitlab.com/Rouxhero) (Main dev)


# Tree Project

## Game 

You'll find [Here](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BGame%5D) all game in dev : 

- [Minecraft Consol](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BGame%5D/Miencraft_Consol) , a MineCraft like game, only on cmd
- [SquareGame](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BGame%5D/SquareGame) , a litle game with a square
- [Import Life](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BGame%5D/importLife) , a project of a universe(not dev yet)

## Utilitaire

[Here](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BUtilitaire%5D), you'll find some python util for the dev, or GTA Server


- [GTA util](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BUtilitaire%5D/Gta%20Util), Some app for GTA server
- [Sniffer](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BUtilitaire%5D/Sniffer) App to clean Computer disk
- [Python Easer](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BUtilitaire%5D/pythonEseaer) Global app to use util(not dev yet)
- [Util](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BUtilitaire%5D/Util) Python package, with a lot of function for python

## Simulator(not dev yet)

[Here](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BSimulator%5D) some test of  simualtion in python

- [Civilation](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BSimulator%5D/Civilisation) a Simulator of people
- [Universe](https://gitlab.com/Rouxhero/python_project/-/tree/master/%5BSimulator%5D/Universe) a simulator of universe